# FLAVi on Linux Ubuntu v18.04

## What is this?

The following are instructions to install FLAVi and its dependencies on a Linux Ubuntu v18.04 (Bionic Beaver). These instructions assume the user has administrative privileges (i.e., can execute `sudo` commands).

## Installing Dependencies on Linux Ubuntu v18.04 (Bionic Beaver)

The following command lines can be execute, in order, on the terminal. They should install al the dependencies for the FLAVi pipeline. These instructions assume that the user is on the `sudo` list. A password will be requested (possibly more than once) to concede administrative priveledges.

```bash
mkdir ~/software
cd ~/software
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install build-essential
sudo apt-get install wise
sudo apt-get install transdecoder
sudo apt-get install ncbi-blast+
sudo apt-get install xorg openbox
sudo apt-get install libx11-dev
sudo apt-get install emboss-lib
wget http://eddylab.org/software/hmmer/hmmer.tar.gz
tar -zxvf hmmer.tar.gz && rm hmmer.tar.gz
cd hmmer-3.2.1/
./configure && make && make check
sudo make install
cd ~/software
wget ftp://emboss.open-bio.org/pub/EMBOSS/EMBOSS-6.6.0.tar.gz
tar -zxvf EMBOSS-6.6.0.tar.gz && rm EMBOSS-6.6.0.tar.gz
cd EMBOSS-6.6.0
./configure && make
sudo make install
cd ~/software
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
sudo add-apt-repository 'deb https://cloud.r-project.org/bin/linux/ubuntu bionic-cran35/'
echo "http://< my.favorite.cran.mirror>/bin/linux/ubuntu trusty/" | sudo tee -a /etc/apt/sources.list
sudo apt update
sudo apt upgrade
sudo apt install r-base
sudo apt install python3-pip
pip3 install biopython
sudo apt-get install mafft
```

### TranslatorX v1.1

You will also need to create the directory `~/software/TRANSLATORX_v1.1` and add there the Perl script `translatorx_vLocal.pl`, which is avaialble at `http://translatorx.co.uk/`.

Modify the permissions to  `translatorx_vLocal.pl` and add a link to it, like so:

```bash
chmod a+rx-w ~/software/TRANSLATORX_v1.1/translatorx_vLocal.pl
ln -s ~/software/TRANSLATORX_v1.1/translatorx_vLocal.pl ~/software/TRANSLATORX_v1.1/translatorx.pl
```

**WARNING:** The `translators_vLocal.pl` looks for the `mafft` executable at `/usr/local/bin/mafft`, but installing `mafft` as described above would put it at `/usr/bin/mafft`. A link could fix this:

```bash
sudo ln -s /usr/bin/mafft /usr/local/bin/mafft
```

## Downloading and configuring the FLAVi pipeline

For the next few steps, it's recommend to replace `~` for the actual path to your home directory (e.g., `/home/username`).

### First, download FLAVi from GitLab and modify the permissions of the Python scripts

```bash
cd ~/software
gi			t clone https://gitlab.com/MachadoDJ/FLAVi.git
cd FLAVi/
chmod a+rx-w *py modules/*py
```

### Second, modify the `configuration.txt` file

Your configuration file should look like this:

```markdown
>genomes=~/software/FLAVi/sample_input/sequence.fasta
>peptides=~/software/FLAVi/peptides/NS5.pep,~/software/FLAVi/peptides/NS3.pep,~/software/FLAVi/peptides/E.pep,~/software/FLAVi/peptides/NS2.pep,~/software/FLAVi/peptides/NS4B.pep,~/software/FLAVi/peptides/NS1
.pep,~/software/FLAVi/peptides/NS4A.pep,~/software/FLAVi/peptides/C.pep,~/software/FLAVi/peptides/pr.pep,~/software/FLAVi/peptides/M.pep
>genewise=/usr/bin
>genewise_handler=~/software/FLAVi/auxiliary_scripts
>blastp=/usr/bin
>hmmscan=/usr/local/bin
>transdecoder=/usr/bin
>protdb=~/software/FLAVi/uniprot/uniprot_sprot.pep
>pfam=~/software/FLAVi/pfam/Pfam-A.hmm
>translatorx=~/software/TRANSLATORX_v1.1/translatorx.pl
>rscript=/usr/bin
>find_outliers=~/software/FLAVi/auxiliary_scripts
>distmat=/usr/local/bin
```

Note that all paths must accuratley match your system's configuration.

### Third, define the `FLAVI_HOME` environmental variable

```bash
printf "\n# FLAVi configuration\nexport FLAVI_HOME=~/software/FLAVi\n" >> ~/.bashrc
source ~/.bashrc
```

### Forth, test

In here, we will prepare a test using only one CPU. You can change that according to the computational resources you have available.

```bash
cd ${FLAVI_HOME}
sed -i -e 's/\/path\/to\/FLAVi/~\/software\/FLAVi/' run_test.sh
sed -i -e 's/NCPUS=6/NCPUS=1/' run_test.sh
chmod a+rx-w *.sh
./run_test.sh
```

## Author

- **Author:** Denis Jacob Machado
- **Email:** `machadodj[at]alumni.usp.br`
- **Last modification:** July 30, 2019