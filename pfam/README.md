Before executing the FLAVi pipeline, you must prepare the Pfam database. The following command line could be executed from this directory to do that:

```bash
$ hmmpress Pfam-A.hmm
```

