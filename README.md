# What is FLAVi?

FLAVi stands for "Fast Loci Annotation of Viruses." The FLAVi pipeline aims to provide an annotation framework for multiple complete or partial genomes of viruses. Currently, FLAVi is designed to annotate the genomes of viruses of the family Flaviviridae only.

The pipeline takes a multi-FASTA file with multiple entries, each corresponding to a complete or partial genome sequence (cDNA).

For a description of the main steps of the FLAVi pipeline, see the "workflow" files that come with FLAVi.

# How to execute

Simply run the FLAVi.py Python script (make it executable and available on your PATH, or call it using python3). For example:

```bash
$ python3 /path/to/FLAVi.py -c configuration.txt
```

# Dependencies

* [genewise](https://www.ebi.ac.uk/~birney/wise2/) (comes with Wise v2.4.1)
* TransDecoder.LongOrfs and TransDecoder.Predict ([TransDecoder v5.5.0](https://github.com/TransDecoder/TransDecoder/wiki))
* [TranslatorX v1.1](http://translatorx.co.uk) [march 2010]
* [blastp](https://blast.ncbi.nlm.nih.gov/Blast.cgi?CMD=Web&PAGE_TYPE=BlastDocs&DOC_TYPE=Download) (comes with BLAST v2.8.1+)
* [hmmscan](http://hmmer.org/) (comes with HMMER v3.1b2)
* [Rscript](https://www.r-project.org/) (R scripting front-end version 3.5.3)
* [mafft](https://mafft.cbrc.jp/alignment/software/) v7.310 (2017/Mar/17)

Also, you may want to have [Biopython](https://biopython.org/) (optional). It is used to generate concatenated gene matrices.

NOTE---FLAVi depends on the genewise application that comes with Wise2. We have not been able to run it using the genewise application from Wise3. Unfortunately, at this time, Wise2 has been deprecated and links to Wise2 source are down.

## Outlier removal using distmat

We decided to remove automatic outlier detection based on aligned sequence similarity. If you want to have that functionality, edit the FLAVi.py script (following the comments inside that script). To remove outliers, you will need distmat (see below).

* [distmat](http://emboss.sourceforge.net/download/) (comes with EMBOSS v6.6.0)

## FLAVI_HOME

Make sure that the system's variable `FLAVI_HOME` is available and that it point to the `FLAVi.py` directory. For example:

```bash
$ export FLAVI_HOME="/path/to/FLAVi.py"
```

## Notes on the installation of dependencies

* It is easier to install `genewise`using **homebrew** (macOS), **apt** (Ubuntu Linux), or other package manager. To install `genewise` with **homebrew**, do this:

```bash
$ brew install brewsci/bio/genewise
```

* TranslatorX is a perl script. Simply save it as `translatorx.pl` in the desired locations
* Newest versions of Blast and Rscript might work, but have not been testes
* It is important to use HMMER v3.1b2 and EMBOSS v6.6.0 (for `distmat`)
* TranslatorX will expect you to have the `mafft` (any version should work, but the pipeline was tested with v7.407) installed in your computer and available on your system's PATH

## Databases

The HMM database has to be prepared with the command `hmmpress`of the same version of the HMM toolkit given in the configuration file (see below).

Example:

```bash
$ ~/sw/HMMER_v3.1b2/bin/hmmpress Pfam-A.hmm
```

The Blast protein database has to be prepared with the command `makeblastdb`of the same version of the Blast toolkit given in the configuration file (see below).

Example:

```bash
$ makeblastdb -in uniprot_sprot.pep -parse_seqids -dbtype prot
```

# Homemade scripts

* `find_outliers.R`: Tukey's test for outliers;
* `genewise_handler.py`: Runs a multifasta file with genomic DNA sequences against a multifasta file with amino acids for a given gene;
* `FLAVi.py`: Main pipeline script.

# Configuration file

The configuration file defines 13 variables. All of them are needed. Please check the original template (`configuration.txt`) and modify it as needed.

# Contact

**Denis Jacob Machado, Ph.D.**

*Department of Bioinformatics and Genomics, College of Computing and Informatics, University of North Carolina at Charlotte.*

Janies Lab: [janieslab.github.io](https://janieslab.github.io/index.html).

Email: dmachado[at]uncc.edu.

# Warning

Note that FLAVi is designed to take multiple complete entries at the same time. The distance among annotated sequences is used to select and remove possible outliers. Therefore, the pipeline might not perform as intended if input genomic sequences are too few or too fragmented. Carefull examination of the output files is recommended.

# What is being annotated

FLAVi annotates 10 genes that compose the entire poliprotein of flaviviruses:

1)  C
2)  pr
3)  M
4)  E
5)  NS1
6)  NS2
7)  NS3
8)  NS4A
9)  NS4B
10) NS5