#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# extract_genes.py
# Reads genome sequences in GenBank format, and exports concatenated gene files (DNA and AA).

# Example: $ python3 extract_genes.py -i 119flavisandoutgroups.gb

# Import modules and libraries
import argparse,re,sys
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.Alphabet import generic_dna

# Set arguments
parser=argparse.ArgumentParser()
parser.add_argument("-i","--input",help="Input file in GenBank format",type=str,required=True)
args=parser.parse_args()

# Define functions
def main():
	# Create dictionary
	dic={}
	# Read GenBank file one entry at a time
	for record in SeqIO.parse(args.input,"genbank"):
		id=record.id
		genome=record.seq
		# Read every feature of each record, one
		for i in [i for i in record.features if i.type=="mat_peptide"]:
			gene=re.sub("[\\\/\s]+","_",i.qualifiers["product"][0].lower())
			gene=re.sub("[\-\'\"\(\)]","",gene)
			try:
				start,end=[int(i) for i in re.compile("\[(\d+):(\d+)\]").findall(str(i.location))[0]]
			except:
				pass
			else:
				if(((end-start)%3)==0):
					seq=Seq(str(genome[start:end]),generic_dna).translate(table=1) # Translate using standard code
					if(gene in dic):
						dic[gene]+=[">{}\n{}\n".format(id,seq)]
					else:
						dic[gene]=[">{}\n{}\n".format(id,seq)]
	# Create output files
	for gene in dic:
		sys.stdout.write("{0} ({1})\n".format(gene,len(dic[gene])))
		handle=open("{}.pep".format(gene),"w")
		handle.write("{}".format("".join(dic[gene])))
		handle.close()
	return

# Execute functions
main()

# Quit
exit()
