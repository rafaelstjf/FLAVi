# Before executing FLAVi

The `README.md` file describes the FLAVi pipeline and its dependencies. Ideally, the pipeline will be at a directory we call `FLAVI_HOME` , which is a system variable that have to be set by the user. For example:

```bash
export FLAVI_HOME="/path/to/FLAVi"
```

Check to see if at least the following files and subdirectories ara available inside `FLAVI_HOME`:

```bash
├── FLAVi.py
├── LICENSE.txt
├── README.md
├── auxiliary_scripts/
│   ├── extract_genes.py
│   ├── find_outliers.R
│   └── genewise_handler.py
├── checklist.md
├── configuration.txt
├── jobs/
│   └── test/
├── modules/
│   ├── Conf.py
│   ├── DistM.py
│   ├── GeneW.py
│   ├── Out.py
│   ├── TransD.py
│   ├── TransX.py
├── peptides/
│   ├── C.pep
│   ├── E.pep
│   ├── M.pep
│   ├── NS1.pep
│   ├── NS2.pep
│   ├── NS3.pep
│   ├── NS4A.pep
│   ├── NS4B.pep
│   ├── NS5.pep
│   └── pr.pep
├── pfam/
│   ├── Pfam-A.hmm
│   └── README.md
├── run_test.sh
├── sample_input/
│   ├── dengue.fasta
│   ├── dengue.fasta.md5
│   ├── dengue.gb
│   ├── sequence.fasta
│   ├── zika.fasta
│   ├── zika.fasta.md5
│   └── zika.gb
└── uniprot/
    ├── README.md
    ├── uniprot_sprot.pep
```

The user may change the contents of `pfam/`, `peptides/`, and `uniprot/` to update or modify these databases, but the appropriate changes must be made in the `configuration.txt` file.