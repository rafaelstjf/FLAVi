#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# genewise_handler.py
# None

# Import modules and libraries
import re,sys,subprocess,argparse

# Set arguments
parser=argparse.ArgumentParser()
parser.add_argument("-i","--input",help="Input genome in fasta format",type=str,required=True)
parser.add_argument("-p","--peptide",help="Input aa sequence in fasta format",type=str,required=True)
parser.add_argument("-g","--genewise",help="Path to genewise",type=str,default="genewise",required=True)
args=parser.parse_args()

# Define functions
def main():
	genome_trgt=parse_gen(args.input)    # Create .FA files from concatenated fasta files
	pep_file=args.peptide
	pep_name,pep_qry=parse_pep(pep_file) # Create .AA files from concatenated fasta files
	for trgt in sorted(genome_trgt):
		gw_list=genewise(pep_qry,trgt)
		if(len(gw_list)>=1):
			best=sorted(gw_list)[-1][2]
			handle=open("{}_{}.gw.out".format(best,trgt),"rU")
			pep,cdna,gff=re.compile("([^\/]+)\/\/",re.M|re.S).findall(handle.read())
			pep=re.sub("\s*\/\/.*","",pep)
			cdna=re.sub("\s*\/\/.*","",cdna)
			gff=re.sub("\s*\/\/.*","",gff)
			handle.close()
			handle=open("{}_{}.gw.pep".format(best,trgt),"w")
			handle.write(pep)
			handle.close()
			handle=open("{}_{}.gw.fasta".format(best,trgt),"w")
			handle.write(cdna)
			handle.close()
			handle=open("{}_{}.gw.gff".format(best,trgt),"w")
			handle.write(gff)
			handle.close()
		else:
			sys.stderr.write("! {} ({})\n".format(trgt,len(gw_list)))
	return

def parse_gen(fasta_file): # Parses a fasta with one or more entries
	genome_trgt=[]
	handle=open(fasta_file,"rU")
	fasta=handle.read()
	handle.close()
# 	for id,nuc in re.compile(">([^\.]+\.\d+)[^\n]*\n([^>]+)",re.M|re.S).findall(fasta):
	for id,nuc in re.compile(">([^\s]+)\s*\n\s*([^>]+)",re.M|re.S).findall(fasta):
		handle=open("{}.FA".format(id),"w")
		entry=">{}\n{}\n".format(id.strip(),nuc.strip())
		handle.write(entry)
		handle.close()
		genome_trgt+=[id]
	return genome_trgt

def parse_pep(pep_file): # Parses a fasta with one or more entries
	pep_qry=[]
	pep_name=re.compile("[^\.]+").findall(pep_file)[0]
	handle=open(pep_file,"rU")
	fasta=handle.read()
	handle.close()
#	for id,aa in re.compile(">[^:]+::(.+?)::[^\n]+\n([^>]+)",re.M|re.S).findall(fasta):
	for id,aa in re.compile(">([^\s]+)\n([^>]+)",re.M|re.S).findall(fasta):
		output="{}_{}".format(pep_name,id)
		handle=open("{}.AA".format(output),"w")
		handle.write(">{}\n{}\n".format(id.strip,aa.strip()))
		handle.close()
		pep_qry+=[output]
	return pep_name,pep_qry

def genewise(pep_qry,trgt):
	gw_list=[]
	for qry in sorted(pep_qry):
		sys.stdout.write("{}\t{}\t".format(qry,trgt))
		cmd="{0} {1}.AA {2}.FA -cdna -pep -gff -tfor > {1}_{2}.gw.out 2> {1}_{2}.gw.err".format(args.genewise,qry,trgt)
		p=subprocess.Popen(cmd, shell=True)
		p.wait()
		handle=open("{0}_{1}.gw.out".format(qry,trgt),"rU")
		gw_out=handle.read()
		handle.close()
		try:
			pep,cdna,gff=re.compile("[^\/]+\/\/",re.M|re.S).findall(gw_out)
		except:
			sys.stdout.write("\tNA\tNA\n".format(qry))
		else:
			if(gff.count("cds")!=1):
				sys.stdout.write("\tNA\tNA\n".format(qry))
			else:
				start,end,score=re.compile("GeneWise\s+match\s+(\d+)\s+(\d+)\s+([^\s]+)").findall(gw_out)[0]
				length=int(end)-int(start)+1
				score=float(score)
				if(score<=10.0):
					sys.stdout.write("\tNA\tNA\n".format(qry))
				else:
					sys.stdout.write("\t{}\t{}\n".format(score,length))
					gw_list+=[[score,length,qry]]
	return gw_list

# Execute functions
main() # Start here

# Quit
exit() # .FA and .AA files can be deleted afterwards

