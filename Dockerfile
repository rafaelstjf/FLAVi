# Use an official Ubuntu 18.04 image as the base image
FROM python:3.9.18-slim-bullseye

# Set environment variables
ENV DEBIAN_FRONTEND=noninteractive

# Create FLAVi Folder
RUN mkdir -p /FLAVi

# Update and upgrade the system
RUN apt-get update && apt-get upgrade -y

# Install dependencies
RUN apt-get install -y \
    build-essential \
    wise \
    transdecoder \
    mafft \
    python3-setuptools \
    python3-pip \
    python3-dev \
    python3-venv \
    ncbi-blast+ \
    xorg \
    openbox \
    libx11-dev \
    emboss-lib \
    hmmer \
    wget \
    perl \
    r-base-core \
    libdb-dev \
    curl \
    && rm -rf /var/lib/apt/lists/*

# Install TranslatorX v1.1
RUN mkdir -p /FLAVi/TRANSLATORX_v1.1 \
    && wget http://161.111.160.230/cgi-bin/translatorx_vLocal.pl -O /FLAVi/TRANSLATORX_v1.1/translatorx_vLocal.pl \
    && chmod a+rx-w /FLAVi/TRANSLATORX_v1.1/translatorx_vLocal.pl \
    && mv /FLAVi/TRANSLATORX_v1.1/translatorx_vLocal.pl /FLAVi/TRANSLATORX_v1.1/translatorx.pl

# Install Transdecoder dependencies

RUN curl -L https://cpanmin.us | perl - App::cpanminus && \
    cpanm install DB_File && \
    cpanm install URI::Escape
# Install Transdecoder
RUN cd /FLAVi && \
    wget https://github.com/TransDecoder/TransDecoder/archive/refs/tags/TransDecoder-v5.7.1.zip && \
    unzip TransDecoder-v5.7.1.zip

# Optional: Install Biopython
RUN python3 -m pip install  --upgrade pip && \
    python3 -m pip install biopython

RUN ln -s /usr/bin/mafft /usr/local/bin/mafft

# Copy the FLAVi pipeline to the container
COPY . /FLAVi

# Make the modules executable
RUN chmod a+rx-w /FLAVi/*.py /FLAVi/modules/*.py

# Set up FLAVi configuration (replace with your actual configuration)
ENV FLAVI_HOME=/FLAVi

# Prepare the test
WORKDIR /FLAVi

# Prepare the HMM database
RUN cd /FLAVi/pfam && \
    hmmpress Pfam-A.hmm

# Prepare the Blast database
RUN cd /FLAVi/uniprot && \
    makeblastdb -in uniprot_sprot.pep -parse_seqids -dbtype prot

# Expose any necessary ports if needed
# EXPOSE <port_number>

# Define the entry point or command to run FLAVi
ENTRYPOINT ["python3", "FLAVi.py"]
