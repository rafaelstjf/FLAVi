#!/usr/bin/env bash

# Name: run_test.sh
# Usage: execute `bash run_test` from the FLAVI_HOME directory
# Dependencies: same as FLAVi

export FLAVI_HOME="/Users/dmachado/Documents/GitLab/FLAVi" # Change this path accordingly
export WD="jobs/test" # Change this path accordingly
export NCPUS=3 # Change the number of threads according to your needs

function prep {
	cd ${FLAVI_HOME}/uniprot
	if [ ! -f ${FLAVI_HOME}/uniprot/uniprot_sprot.pep.phr ] ; then
		makeblastdb -in uniprot_sprot.pep -parse_seqids -dbtype prot
		wait
	fi
	cd ${FLAVI_HOME}/pfam
	if [ ! -f ${FLAVI_HOME}/pfam/Pfam-A.hmm.h3f ] ; then
		for i in Pfam-A.hmm.* ; do
			rm ${i}
		done
		hmmpress Pfam-A.hmm
		wait
	fi
	cd ${FLAVI_HOME}/
	if [ ! -d ${WD} ] ; then
		mkdir -p ${WD}
	fi
}

function clean {
	for i in ${WD}/* ; do
		if [ -d ${i} ] ; then
			rm -r ${i}
		elif [ -f ${i} ] ; then
			rm ${i}
		fi

	done
}

function annotate {
	printf "Start date: `date`.\n"
	SECONDS=0
	cd ${FLAVI_HOME}
	python3 ${FLAVI_HOME}/FLAVi.py -c ${FLAVI_HOME}/configuration.txt -p ${NCPUS} -o ${FLAVI_HOME}/${WD}
	wait
	duration=$SECONDS
	printf "End date: `date`.\n"
	printf "Elapsed time: $(($duration / 60)) minutes and $(($duration % 60)) seconds.\n"
}

prep
clean
annotate

exit
