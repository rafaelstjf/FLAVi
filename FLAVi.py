#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# FLAVi.py: Annotation pipeline for Flaviviridae complete genomes

# Last edited: October 5, 2023
# Edited by: Denis Jacob Machado

####
## IMPORT LIBRARIES
####

import argparse
import glob
import os
import re
import sys
from shutil import copyfile

####
## IMPORT SYSTEM'S VARIABLE
####

try:
	FLAVI_HOME = os.environ.get('FLAVI_HOME')
except:
	sys.stderr.write("! ERROR: Could not find the environmental variable FLAVI_HOME. Create it by typing 'export FLAVI_HOME = /full/path/to/cma/directory'\n")
	exit()

####
## IMPORT DEDICATED MODULES
####


sys.path.append("{}/modules/".format(FLAVI_HOME)) # !!! Be sure to export CMA_HOME as a sytem's variable !!!
import Check
import Concat
import Conf
#import DistM
import GeneW
#import Out
import Report
import SingleSeqReport
import TransD
import TransX

####
## SET ARGUMENTS
####

parser = argparse.ArgumentParser()
parser.add_argument("-c", "--configuration", help = "Configuration file [default = configuration.txt]", type = str, default = "configuration.txt", required = False)
parser.add_argument("-o", "--output", help = "Path to output directory [default = jobs/test]", type = str, default = "jobs/test", required = False)
parser.add_argument("-p", "--processors", help = "Number of CPUs [default = 4]", type = int, default = 4, required = False)
parser.add_argument("-v", "--verbose", help = "Increase output verbosity", action = "store_true")
args = parser.parse_args()

####
## EXECUTE
####

# Preamble

arguments = Conf.main(args.configuration) # Read configuration file using dedicated module
count = 0
nprocs = args.processors
peptides = arguments["peptides"]

# Check if input file appears to be in FASTA format and count the number of entries

test, nseqs = Check.main(arguments["genomes"])
if(test == False):
	sys.stderr.write("! ERROR: Please check file {}.\n".format(arguments["genomes"]))
	exit()
elif(args.verbose):
	sys.stdout.write("> File {} has {} entries.\n".format(arguments["genomes"], nseqs))

# Processing N genes at a times, were N is the number od processors

while (len(peptides[0 + count * nprocs : (count + 1) * nprocs]) >= 1):
	if(args.verbose):
			sys.stdout.write("[ part {0} ]\n".format(count + 1))

# Select next set of genes

	selected = peptides[0 + count * nprocs : (count + 1) * nprocs] # The selected peptides will be processed in parallel
	peptides_dic = {}
	for pep_file in selected:
		pep_name = re.compile("([^\.\/]+)\..+").findall(pep_file)[0].strip()
		peptides_dic[pep_file] = pep_name

# Execute additional functions from dedicated modules

	if(args.verbose):
		sys.stdout.write("> Running GeneWise\n")
	GeneW.main(arguments["genewise_handler"], arguments["genewise"], arguments["genomes"], selected, peptides_dic, args.output, args.verbose)
	if(args.verbose):
		sys.stdout.write("> Running TransDecoder\n")
	TransD.main(arguments["transdecoder"], arguments["blastp"], arguments["protdb"], arguments["hmmscan"], arguments["pfam"], peptides_dic, args.processors, args.output, args.verbose)
	
	if(args.verbose):
		sys.stdout.write("> Running TranslatorX\n")
	TransX.main(peptides_dic, arguments["translatorx"], args.output, args.verbose)
#	if(args.verbose):
#		sys.stdout.write("> Running DistMat\n")
#	DistM.main(peptides_dic, arguments["distmat"], args.output, args.verbose)
#	if(args.verbose):
#		sys.stdout.write("> Checking for outliers\n")
#	Out.main(arguments["rscript"], arguments["find_outliers"], peptides_dic, args.output, args.verbose)

# Increment counting

	count += 1

# Report annotation and a few statistics

for pep_file in peptides:
	pep_name = re.compile("([^\.\/]+)\..+").findall(pep_file)[0]
	final_fa = "{0}/{1}/{1}.final.fasta".format(args.output, pep_name)
	align_fa = "{0}/{1}/{1}.align.fa".format(args.output, pep_name)
	if (not os.path.isfile(final_fa)) and (os.path.isfile(align_fa)):
		copyfile(align_fa, final_fa)

Report.main(peptides, nprocs, args.output)

try:
	from Bio import SeqIO
except:
	sys.stderr.write("! WARNING: Biopython is not available and concanted alignments will not going to be generated.\n")
else:
	Concat.main(peptides, args.output)

####
## QUIT
####

exit()
