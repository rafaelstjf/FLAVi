>ref-AY898809-NS2
LRADMVDPFQLGLLVVFLATQEVLRKRWTARMTIPAVVAAFLVMILGGITYVDIARYLILVGAAFMEHNNGGDVVHLALIAAFKIQPGFLVMSMMKSHWTNQENLALVLGAAFFQMAATDLQFNMPDLLNAAAIAWMIIRALARPSASALAMPMLALLAPGMRLLYMDTYRIVLLLIGVSSLLNEKRKAAEKKKGAVLMSLALTTTGWCSPSILAAGLMVFNPNKRRGWPATEVLTAVGLMFAIVGGLAELDVESMAIPFTIAGLMLVTYVISGKSVDMWLERAADITWEQDAAITGTSERVDVCLDDDGDFHLINDPGVPWKVWLLRMTCIGLASVTPWAIIPAAVGYWLTLKYAKR
>ref-JF416958-NS2
NGAMLSEGGVPGIVAVFVVLELVIRRRPTTGTSVVWCGMVVLGLVVTGLVTIEGLCRYVVAVGILMSMELGPEIVALVLLQAVFDMRTGLLVAFAVKRAYTTREAVATYFLLLVLELGFPEASLSNIWKWADSLAMGALILQACGQEGRTRVGYLLAAMMTQKDMVIIHTGLTIFLSAATAMAVWSMIKGQRDQKGLSWATPLAGLLGGEGVGLRLLAFRKLAERRNRRSFSEPLTVVGVMLTVASGMVRHTSQEALCALVAGAFLLLMMVLGTRKMQLTAEWCGEVEWNPDLVNEGGEVNLKVRQDAMGNLHLTEVEKEERAMALWLLAGLVASAFHWAGILIVLAVWTLFEMLGSGRR
>ref-NC_000943-NS2
FNGDMIDPFQLGLLVMFLATQEVLRKRWTARLTLPAAVGALLVLLLGGITYTDLVRYLILVGSAFAESNNGGDVIHLALIAVFKVQPAFLVASLTRSRWTNQENLVLVLGAAFFQMAASDLELTIPGLLNSAATAWMVLRAMAFPSTSAIAMPMLAMLAPGMRMLHLDTYRIVLLLIGICSLLNERRRSVEKKKGAVLIGLALTSTGYFSPTIMAAGLMICNPNKKRGWPATEVLTAVGLMFAIVGGLAELDIDSMSVPFTIAGLMLVSYVISGKATDMWLERAADVSWEAGAAITGTSERLDVQLDDDGDFHLLNDPGVPWKIWVLRMTCLSVAAITPRAILPSAFGYWLTLKYTKR
>ref-NC_001437-NS2
FNGEMVDPFQLGLLVMFLATQEVLRKRWTARLTIPAVLGALLVLMLGGITYTDLARYVVLVAAAFAEANSGGDVLHLALIAVFKIQPAFLVMNMLSTRWTNQENVVLVLGAALFQLASVDLQIGVHGILNAAAIAWMIVRAITFPTTSSVTMPVLALLTPGMRALYLDTYRIILLVIGICSLLQERKKTMAKKKGAVLLGLALTSTGWFSPTTIAAGLMVCNPNKKRGWPATEFLSAVGLMFAIVGGLAELDIESMSIPFMLAGLMAVSYVVSGKATDMWLERAADISWEMDAAITGSSRRLDVKLDDDGDFHLIDDPGVPWKVWVLRMSCIGLAALTPWAIVPAAFGYWLTLKTTKR
>ref-NC_001474-NS2
GHGQVDNFSLGVLGMALFLEEMLRTRVGTKHAILLVAVSFVTLITGNMSFRDLGRVMVMVGATMTDDIGMGVTYLALLAAFKVRPTFAAGLLLRKLTSKELMMTTIGIVLLSQSTIPETILELTDALALGMMVLKMVRNMEKYQLAVTIMAILCVPNAVILQNAWKVSCTILAVVSVSPLLLTSSQQKTDWIPLALTIKGLNPTAIFLTTLSRTSKKRSWPLNEAIMAVGMVSILASSLLKNDIPMTGPLVAGGLLTVCYVLTGRSADLELERAADVKWEDQAEISGSSPILSITISEDGSMSIKNEEEEQTLTILIRTGLLVISGLFPVSIPITAAAWYLWEVKKQR
>ref-NC_001475-NS2
GSGKVDNFTMGVLCLAILFEEVLRGKFGKKHMIAGVFFTFVLLLSGQITWRDMAHTLIMIGSNASDRMGMGVTYLALIATFKIQPFLALGFFLRKLTSRENLLLGVGLAMATTLQLPEDIEQMANGVALGLMALKLITQFETYQLWTALVSLTCSNTIFTLTVAWRTATLILAGVSLLPVCQSSSMRKTDWLPMTVAAMGVPPLPLFIFSLKDTLKRRSWPLNEGVMAVGLVSILASSLLRNDVPMAGPLVAGGLLIACYVITGTSADLTVEKAPDVTWEEEAEQTGVSHNLMITVDDDGTMRIKDDETENILTVLLKTALLIVSGIFPYSIPATLLVWHTWQKQTQR
>ref-NC_001477-NS2
GSGEVDSFSLGLLCISIMIEEVMRSRWSRKMLMTGTLAVFLLLTMGQLTWNDLIRLCIMVGANASDKMGMGTTYLALMATFRMRPMFAVGLLFRRLTSREVLLLTVGLSLVASVELPNSLEELGDGLAMGIMMLKLLTDFQSHQLWATLLSLTFVKTTFSLHYAWKTMAMILSIVSLFPLCLSTTSQKTTWLPVLLGSLGCKPLTMFLITENKIWGRKSWPLNEGIMAVGIVSILLSSLLKNDVPLAGPLIAGGMLIACYVISGSSADLSLEKAAEVSWEEEAEHSGASHNILVEVQDDGTMKIKDEERDDTLTILLKATLLAISGVYPMSIPATLFVWYFWQKKKQR
>ref-NC_001563-NS2
YNADMIDPFQLGLMVVFLATQEVLRKRWTAKISIPAIMLALLVLVFGGITYTDVLRYVILVGAAFAEANSGGDVVHLALMATFKIQPVFLVASFLKARWTNQESILLMLAAAFFQMAYYDAKNVLSWEVPDVLNSLSVAWMILRAISFTNTSNVVVPLLALLTPGLKCLNLDVYRILLLMVGVGSLIKEKRSSAAKKKGACLICLALASTGVFNPMILAAGLMACDPNRKRGWPATEVMTAVGLMFAIVGGLAELDIDSMAIPMTIAGLMFAAFVISGKSTDMWIERTADITWESDAEITGSSERVDVRLDDDGNFQLMNDPGAPWKIWMLRMACLAISAYTPWAILPSVIGFWITLQYTKR
>ref-NC_001564-NS2
QSDFRRVSPRAGVGFDRSLLNLLCLAISLQLIGAKTRTSTLTRLFLTILAMALFGLPNLFSSVGLSAWVLLVASSSSQPQDLLMNLWIILQTGSSAVLLLGYMIRKKLSIVLGVHHLVTLMCIQFLFSAVDRYQKYLYGLLELMASVVLLSAYKSVLQALPPEVLCFSLIMGWKTALSLATVVSLIFSLNAMYKYACQYHNPRNGYRDSGANLWFWTVSLASAGGIWAAEKAHQPTVAAVLAFTMIVLFLYMEQTNVSMELEFISAGETPEGVSTENDDGMNIPDLKGRYGEDGIVVGAASSSGHLPELVFVFLLGFAVTSTSYFLGALYLLIATSTNLPVTIIRMLRMKLTANNR
>ref-NC_001672-NS2
DNGELLSEGGVPGIVALFVVLEYIIRRRPSTGTTVVWGGIVVLALLVTGMVRIESLVRYVVAVGITFHLELGPEIVALMLLQAVFELRVGLLSAFALRRSLTVREMVTTYFLLLVLELGLPGASLEEFWKWGDALAMGALIFRACTAEGKTGAGLLLMALMTQQDVVTVHHGLVCFLSVASACSVWRLLKGHREQKGLTWVVPLAGLLGGEGSGIRLLAFWELSAHRGRRSFSEPLTVVGVMLTLASGMMRHTSQEALCALAVASFLLLMLVLGTRKMQLVAEWSGCVEWYPELVNEGGEVSLRVRQDAMGNFHLTELEKEERMMAFWLIAGLAASAIHWSGILGVMGLWTLTEMLRSSRR
>ref-NC_001809-NS2
DNGELLSEGGVPGIVALFVVLECIIRRRPSTGVTVVWGGVVVLALLVTGMVRIESLVRYVVAVGIAFHLELGPETVALMLLQAVFELRVGLLSAFALRRGLTVREMVTTYFLLLVLELGLSSAGLGDLWKWSDALAMGALIFRACTAEGKTGTGLLLIALMTQRDVVTVHHGLVCFLAAAAACSVWRLLRGHREQKGLTWIIPLARLLGGEGSVIRLLAFWELAAHRGRRSFSEPLTVVVVMLTLASGMMRHTSQEALCALAVASFFLLMLVSGTRKMQLVAEWSGCVEWHPETVNEGGEISLRVRQDSMGNFHLTELEKEERMMAFWLLAGLVASALHWSGILGVMGLWTLTEIMRSSRR
>ref-NC_002031-NS2
GEIHAVPFGLVSMMIAMEVVLRKRQGPKQMLVGGVVLLGAMLVGQVTLLDLLKLTVAVGLHFHEMNNGGDAMYMALIAAFSIRPGLLIGFGLRTLWSPRERLVLTLGAAMVEIALGGVMGGLWKYLNAVSLCILTINAVASRKASNTILPLMALLTPVTMAEVRLAAMFFCAVVIIGVLHQNFKDTSMQKTIPLVALTLTSYLGLTQPFLGLCAFLATRIFGRRSIPVNEALAAAGLVGVLAGLAFQEMENFLGPIAVGGLLMMLVSVAGRVDGLELKKLGEVSWEEEAEISGSSARYDVALSEQGEFKLLSEEKVPWDQVVMTSLALVGAALHPFALLLVLAGWLFHVRGARR
>ref-NC_002640-NS2
GQGTSETFSMGLLCLTLFVEECLRRRVTRKHMILVVVITLCAIILGGLTWMDLLRALIMLGDTMSGRIGGQIHLAIMAVFKMSPGYVLGVFLRKLTSRETALMVIGMAMTTVLSIPHDLMELIDGISLGLILLKIVTQFDNTQVGTLALSLTFIRSTMPLVMAWRTIMAVLFVVTLIPLCRTSCLQKQSHWVEITALILGAQALPVYLMTLMKGASRRSWPLNEGIMAVGLVSLLGSALLKNDVPLAGPMVAGGLLLAAYVMSGSSADLSLEKAANVQWDEMADITGSSPIVEVKQDEDGSFSIRDVEETNMITLLVKLALITVSGLYPLAIPVTMTLWYMWQVKTQR
>ref-NC_003635-NS2
SDGEKPLFEWCSFALLTTIYLLFMAGKNTTALIPFVAFLGAILYGSGFLTANLVMKYVAAFYLNSVLEKSEPMLWAYMMEVVFDLRPGMLLGLMLNKTWKFERLVAMIGCLSFLQQATIEWRTAWVLLDCIAVLVVFQSQGSKIKGHLLNIVLLNLSWVTSEHLIMMVKLTGFLLLLSDSGVFERSSWQQKTRTVLTAGMMTLNTPYSFLVAAWPLPRHIRGVDYVAILGILMGVGASVARNGLESELWVMGIMALVLLVFVLQLTTGELLAEWDSHYSWKQNCDQASGSVDLSVKRLPDGRLVNITEKEESRMETLILGVGMIATEFHWVGLPLTVLVLGLRRWIVNGKEQR
>ref-NC_003675-NS2
GSMTALEAQSWGVLSVMLLFYMGFLPKIAKERWSVPTAFMFFVFAITGLIDCRDFLRYLLAVGTTFAWQFPEPMLWMVAMQTVFMLRPALTVGILFARSWKFERILALASLLQVFQHVTMEWESVWKVLDSLGFVVYGFLLSKSGMQSHVFMLFVVSLTWMDSIVMQHALALGGVMLLMSISPKFMQSDWIQKTQLTILGGLKILNAPFYVVALIFYTCFNDYMHRGQRATDYTAILGIVMAIGAAICKNGLESSGWMIGIMGIFLVFFLLQLSKGEMTAEWAGYCEWKKDCDKSVGSLSLEVKRMSDGRLVNLSKDKESMTEVIMVSVGMIVTGFHWIGIPLTIAMLGIKKCYDATQR
>ref-NC_003676-NS2
FDEEPVWKPWGIMGLIVLLYMGFFPSLGNGKFSMGFGMAVFLLCVTGMLSLTELAKYTMLLMAVYAERFDEPCLWAMTVSTVFDLRPGAILLLVFTWPWHFQRVVAAASVTNVLSLMITEGVWAILDGVGLVLYCYLLTRSGKKSMWGLLFVSASRLITEGSLQVAWILVGALTMLCSSRHFLEGDWVQKAGYLCAGYHQVVSSPWRVIPMLWNEMASRKRSGQRSVDPIAIVVTLMALGAAVAKDGTAVQPLTLGLFSAVILFLLLQLTGGPLIAEWAGYADWKSNALRYSGHLDLEVKKLSDGRLVNTAKENTTTTDGLILGAGMLASGFSWVAVPVTFAILALRQWNRSIQK
>ref-NC_003687-NS2
DNGALLSEGGVPGLVAVFVLMEFLLRRRPGSVTSILWGGILMLGLLVTGLVRVEEIVRYVIAVGVTFHLELGPETMVLVMLQAVFNMRTCYLMGFLVKRVITTREVVTVYFLLLVLEMGIPEMNFGHLWEWADALAMGLLIIKASAMEDRRGLGFLLAGLMTQRHLVAVHHGLMVFLTVALAVVGRNIYNGQKERKGLCFTVPLASLLGGSGSGLRMLALWECLGGRGRRSLSEPLTVVGVMLAMASGLLRHSSQEALLALSAGSFLILMLILGTRRLQLTAEWAGVVEWNPELVNEGGEVSLKVRQDAMGNLHLTEVEREERRLALWLVFGLLASAYHWSGILVTMGAWTVYELFSSTRR
>ref-NC_003690-NS2
DNGALLSEGGVPGVVALFVVLELVIRRRPATGGTVIWGGIAILALLVTGLVSVESLFRYLVAVGLVFQLELGPEAVAMVLLQAVFEMRTCLLSGFVLRRSITTREIVTVYFLLLVLEMGIPVKGLEHLWRWTDALAMGAIIFRACTAEGKTGIGLLLAAFMTQSDMNIIHDGLTAFLCVATTMAIWRYIRGQGERKGLTWIVPLAGILGGEGSGVRLLAFWELAASRGRRSFNEPMTVIGVMLTLASGMMRHTSQEAVCAMALAAFLLLMLTLGTRKMQLLAEWSGNIEWNPELTSEGGEVSLRVRQDALGNLHLTELEKEERMMAFWLVVGLIASAFHWSGILIVMGLWTISEMLGSPRR
>ref-NC_003996-NS2
SKMNKDNIQDLITKNLNVLMVIDNNDESSIRALIGDKMLFCEANWVDCNTHDYELSFWTSALITTKSRSYEHKPCKIQPKDEIFEDADSFFSDEKVVLTTAGISRDNVNFYWLFTFFLGWILFEWAVIVDLGSIFLYNIKKRWTVLLIIPTVFQLMVNGIITSIVSITVFFCGSMKNTLVTATDIVEGINLGHWKRTAMVTLMVSGFFKNEFLSLLMILNFTLNWTHFVPILMCLPHFLGLPNPWIWLGVLVLALIGLTNGKLELDFQNKLVPLPQKKFETQLQLGKVYWTASGIEVEIREEIRMAKYDYPLLALYLTLVFSASLIHWSFGITTLMLGGSLFNLRDK
>ref-NC_004119-NS2
QNEMSPLDARCWGVFSILVLYYLGILTKIEKGNWGPSLIVTISIMMWLHIIDVTELLHYLLAVGYTFVWQTSEPMLWIVAMQAIFQLRPGFAVGFALCKTWRFERIVGMVACMWTIQELTTPYEAFWRFMDALGLLVYAYSASHSTRQSYLYVVLMLSLTKIQNATIRLAVGMFGTMLLMLVGRKFAESDWLQKTKVTIAGATRLMELPYFPLVVSYFMRGQPSKRATDYMSILGVVMALGAAVCRNGINTDPWIIGVMALVLLFFLFQVSSGEMVAEWAGYHEWKKDCPKSVGSISLEVKRMADGRLINMSKEKDSLWEMGIVGCGMVVTAMHWIGIPLTIVALAIKSGLDGKRR
>ref-NC_004355-NS2
DNGAMLSEGGVPGIVAVFVVLELVIRRRPTTGTSVVWCGVVVLGLVVTGLVTIEGLCRYVVAVGILMSMELGPEIVALVLLQAVFDMRTGLLVAFAVKRAYTTREAVVTYFLLLVLELGFPEASLSNIWKWADSLAMGTLILQACSQEGRARVGYLLAAMMTQKDMAIIHTGLTIFLSAATAMAVWSMIKGQRDQKGLSWATPLVGLFGGEGVGLRLLAFRRLAERRNRRSFSEPLTVVGVMLTVASGMVRHTSQEALCALVAGAFLLLMMVLGTRKMQLIAEWCGEVEWNPDLVNEGGEVNLKVRQDAMGNLHLTEVEKEERAMALWLLAGLVASAFHWAGILIVLAIWTFFEMLSSGRR
>ref-NC_005064-NS2
CSDFRSSNSRTGVGFDRSILNLLCLAVSLQLIGAKTRVSTLTRLVLTILAMLIFGMPNIFSSVGLSAWVLLAASSSLRPRDVLMNLWIILQTGSSAVLLLGFMIRKRMSIVLGTQHMVALVCVHFLFSVVDRHQKTLYGLLELSAAAILIGAYNGISQTLPPEVLVFCLVMGWKTSLAILTVVSLMFSLHACYRFVVKFHSTKNGYRDYGASSWFWIVSFASAGAIWAAERAQQPTIAATLAFTMIVLFLFMDQSSVSMNLEFLSKGEIPEGVRIEDDEGRDLADLRGRYGDEGIVVGTQQTSMHLPEMVIVVLVGCALTSVSFFLGALYVVVASSTNLPLHVVRYIRIRLSEQNR
>ref-NC_006551-NS2
HRSDMIDPFQLGLLVMFLATQEVLRKRWTARLTVPAIVGALLVLILGGITYTDLLRYVLLVGAAFAEANSGGDVVHLALIAAFKIQPGFLAMTFLRGKWTNQENILLALGAAFFQMAATDLNFSLPGILNATATAWMLLRAATQPSTSAIVMPLLCLLAPGMRLLYLDTYRITLIIIGICSLIGERRRAAAKKKGAVLLGLALTSTGQFSASVMAAGLMACNPNKKRGWPATEVLTAVGLMFAIVGGLAELDVDSMSIPFVLAGLMAVSYTISGKSTDLWLERAADITWETDAAITGTSQRLDVKLDDDGDFHLINDPGVPWKIWVIRMTALGFAAWTPWAIIPAGIGYWLTVKYAKR
>ref-NC_007580-NS2
GVAGGMEPFQLGLLVAFIATQEVLKRRWTGKLTLTSLAVCLALLIFGNLTYMDLVRYLVLVGTAFAEMNTGGDVIHLALVAVFKVQPAFLAGLFLRMQWSNQENILMVIGAAFLQMAANDLKLEVLPILNAMSIAWMLIRAMKEGKVAMYALPILCALTPGMRMAGLDVIRCLLLIIGIVTLLNERRESVAKKKGGYLLAAALCQAGVCSPLIMMGGLILAHPNGKRSWPASEVLTGVGLMCALAGGLLEFEETSMVVPFAIAGLMYITYTVSGKAAEMWIEKAADITWEQNAEITGTSPRLDVDLDSHGNFKLLNDPGAPVHLFALRFILLGLSARFHWFIPFGVLGFWLLGKHSKR
>ref-NC_008604-NS2
SYPDFRLSPSVEGVSPLLVGALLHLLTIRTKHRWAQRTCGTWILFLLFGVPSNTYVGWSWIGLSYSLAAVPNGTALLVHFWLAVQFSSSHLFFLGWALRQRVRSSAGYALTVFFAEWLLLKLRQLWESTYLLDHVLFPMYVMLAFNLKSQFVPVDSMVLLNYVVTHPAVATAMVTGGALVIYSIRVYKSWGCSPNLWRSGLRASRRSLVAGMCLAGLYVLSTCLELYQMPTTASVVFLGGLLIGIVTRMAPPAHLELVPVAGMGVPLDCEEEPTTLPSGLEGTYGPDGVEFTNLTDNSRVSTGLLVYVGCMGIMAINTYVGVILMCACWWTNAPEWLPLYVAGVSVFR
>ref-NC_009026-NS2
GDGNEIDTFSLGLLVAMLVTQEGLRKRWATRHIMVASLTMLAAMVTGHITYRDLLRYVVLLGATFAQINDGGDVMHLALVAVFKVQPGFLLGFLLRRRWTPRESMLLAISACFLHLVFSELSTDITTLAHNFSLALLILRAIIQTDVSSVTLPVLSMMAPSFQLSVLGTFRMAVAVYVIVNLMMSKRNDAVKKAAPSVVAAALGQFGMVNATAALGTLYVLEKHGKRSWPPSEIFSAVGVLCALVGALGNVQSTPLAGPMAACGLLIAAYVVTGKSTDIEIERAGLISWSEDAEVSGSSPRVDVALDENGDFSLIDGQGPSLESVILKTALVAFSGLFPVSIPFCAAAWYLHGKSGRR
>ref-NC_009028-NS2
GNGQTIEPFQLGILMAFVFTQEVLRRRWTANLALPTSALLMACFIFGGFTYLDLFRYFILVGAAFAEANSGGDVVHLAMIAAFNIQPVALVTTFFRKNWTNRENMILIIAAACTQMACMELKIELFHVMNSLSLAWMILKALTTGTTSTLAMPFLAALSPPMNWLGLDVVRCLLIMAGVAALISERRESLAKKKGALLISAALALTGAFSPLVLQGALMFTQSLGKRGWPASEVLTAVGMTFALAGSVARLDGGTMAIPLATMAILAVAYVLSGKSTDMWLERCADISWINEAEITGTSPRLDVELDSNGDFKMINDPGVPMWMWTCRMGLMAMAAYNPVLIPVSMAGYWMTVKIHKR
>ref-NC_009029-NS2
GRGDGVDNLSLGLLVLTIALQEVMRKRILGRHITWMVIAVFMAMILGGLSYRDLGRYLVLVGAAFAERNSGGDLLHLVLVATFKVKPMALLGFVLGGRWCRRQSLLLSIGAVLVNFALEFQGGYFELVDSLALALLFVKAVVQTDTTSVSLPLLAALAPAGCYTVLGTHRFIMLTLVLVTFLGCKKTASVKKAGTAAVGVVLGMVGMKTIPMLGMLMVTSRARRSWPLHEAMAAVGILCALFGALAETEVDLAGPLAAAGLIVMAYVISGRSNDLSIKKVEDVKWSDEAEVTGESVSYHVSLDVRGDPTLTEDSGPGLEKVLLKVGLMAISGIYPVAIPFALGAWFFLEKRCKR
>ref-NC_009942-NS2
YNADMIDPFQLGLLVVFLATQEVLRKRWTAKISMPAILIALLVLVFGGITYTDVLRYVILVGAAFAESNSGGDVVHLALMATFKIQPVFMVASFLKARWTNQENILLMLAAVFFQMAYHDARQILLWEIPDVLNSLAVAWMILRAITFTTTSNVVVPLLALLTPGLRCLNLDVYRILLLMVGIGSLIREKRSAAAKKKGASLLCLALASTGLFNPMILAAGLIACDPNRKRGWPATEVMTAVGLMFAIVGGLAELDIDSMAIPMTIAGLMFAAFVISGKSTDMWIERTADISWESDAEITGSSERVDVRLDDDGNFQLMNDPGAPWKIWMLRMVCLAISAYTPWAILPSVVGFWITLQYTKR
>ref-NC_012532-NS2
GSTDHMDHFSLGVLVILLMVQEGLKKRMTTKIIMSTSMAVLVVMILGGFSMSDLAKLVILMGATFAEMNTGGDVAHLALVAAFKVRPALLVSFIFRANWTPRESMLLALASCLLQTAISALEGDLMVLINGFALAWLAIRAMAVPRTDNIALPILAALTPLARGTLLVAWRAGLATCGGIMLLSLKGKGSVKKNLPFVMALGLTAVRVVDPINVVGLLLLTRSGKRSWPPSEVLTAVGLICALAGGFAKADIEMAGPMAAVGLLIVSYVVSGKSVDMYIERAGDITWEKDAEVTGNSPRLDVALDESGDFSLVEEDGPPMREIILKVVLMAICGMNPIAIPFAAGAWYVYVKTGKR
>ref-NC_012533-NS2
GSGSGVDSLSLGAVVVFLALQEGLKKRYTARGIMVAGVALLAAWLVGGVTLMDLGRYMFLVGTAFAEVNSGRDVAHLALIATFKVRPALLTTYLWRLHWGTREEIGLAVAACLMHLSVAGLEWNIPELLDAMAMAVLVLRASADCRAKNWSLVGLALLTQEGLRNVMPAWKGAMLGIAAGIILSKKSSSLKKSAPLGLAWIMWGAGRLSPLAAVPLLLTGEKRRSWPPTEALSAVGMLGLVMGATMKPGAELSGPLVASGLLLVCYIISGKGTDLFLERAAELSWCPDAAVSGTSPRLDVKITEGGDMELIDDKGPSTESIVLKATLVAVCGMWPLAIPFAMGVWMLYDKKAKR
>ref-NC_012534-NS2
YDGAGMEPFQLGVLMMFVATQEVLRKRWTGRMAGPALVGLLCAMIFGGITYRDMVKYVLLVAAAFAESNSGGDVIHLALIATFNVQPGLLVAYSLRRKWSNQDATLLGVALAMITMALHDWNMTIPSLLNSGATAWLLLRAVSEGTVSAACIPILGLLAPGMKIIGIDVFRIGLLIIGVMSLLKERNNAIAKKKGGVLIGLALAQDGWVNPLVYAGLTLALKPSNRRGWPVSEALTAVGLTFALAGGIAHFDEGDMAIPLAVGGIMLVVAVVTGFTTDLWLERAGDISWIEDAQITGSSQRYDVEIDCDGNMKLMNDQGVPFSIWALRTGLILASAYNPYILPVTLGAYWMTTHSPKR
>ref-NC_012671-NS2
LLQDFRDSLPVDGSSPLLVGVLLHLLTIRTRHRWGQRTIGTWLLFVVFGIPAGSWVGWSWMGLSYSLAAVPNGSALLVHFWLALQFSTSHLFFLGWLLRKRVESSIEYALTVFAAEWVILKLQMLIKQASHLEHVLFPLYVKLALSMRSQFAPVDSMIALNYVVTHPLMAALVSLGVALVIFVTHVCANWRRSPNLWKSGLRASKRSALLGVYFVVLYVVSVLLRFTGLESSAVAVFLGGLLIGLVTRLVPPTKYVLVPVPGTSVPSNCEESFTRLPPGLEGVYGPSGVEFLNYTDVGLVSVAILMFTGCLGVTLMNPLLGITSMLFCWFTEAYVWLPRLIFGTSNRR
>ref-NC_012932-NS2
TADFHRRNPRSGGGFDRSVLNLLCLAIALQLIGARTRATTWSRLILTTVAMLTFGLPNIFSSVGLSAWVLLVASSAYQPIDVVMNLWIILQTGSSAVILLGFMVRRKLSIVLGHHHLMASICLQFLFWVVERQQRAFSVFLEAVAAIVLIGAYRGMTQDLPPEILTFCLIMGWKTALALGTVALLTQGTSAFYKWTQKSLESKSSYRTSGRSAWFWTISCASAGAIWAAERADHPSAAAVLALVTIIAFLYMDQANVTMELEFLSTGDVPDGIALEEDEGGNFRDLRGTYSDEGITIGQDMGSAQIPETMVIILIGCALTSASLFVGALYTILAISTNIPRNLFRLCRLKINEHCR
>ref-NC_015843-NS2
FQGGGMEPMQLGMLVMIVAAQEILRRRMTAPIAWSALLLLMALVLFGGITYSDLVKYVILVAAAFAESNTGGDIVHLAMVAAFNIQPGLLIGFLLRRKWSNQESRLLGVALALITVAMRDLNMSIPTLLNSGAMAWLLLRAVFEGTVSSFALPLVSLLAPGLRIVGIDVVRIGVLTLGILSLLKERSNAMAKKKGGMLLGVACATAGIASPLVFAGLHMVLKPVTRRGWPVSEALTAVGLTFALAGGIAQFDDSSMAIPLAVGGIMLVVAVVTGFSTDLWLEKASDISWSEEARVTGASQRFDVEIDQDGNMRLLNDPGVSLGVWAFRTGLILLSSYNPYFLPLTLAGYWMTTKAKQR
>ref-NC_016997-NS2
GAVHGLDNFSLGLLVMMLVTQEGLKRRFTSKHIMLAGIGLLICVIMGDLTYTDLARYAVLLGTTFAEMNSGGDVMHLALVATFKVKPAYLLGFLFRMRWSPRESFMLVCGAILMQTGMGVLACHHMMDYVHACALGWLLVRSLVVPGMVSKALPLLCCLAPLPLAVMTNATRASVATLAVGTMLAGAKGKSVKKSLPWFSALIASWMGCNPLGMMVFASFMRKHGKRSWPAGEVMAAVGIVCALAGAITENQGEVAGPAAAAALIFTAYAISGKANDIFLEKAGEMTWSHDAQLSGSSPRVDVKVGESGDFRLRHESEDSWLKTGVTAGCLIMAGFHPLAIPVAGLIWYGYVKHDRR
>ref-NC_017086-NS2
GTFQGIDDFSLGLLVLIIFVQEGLKRRMTSRYIMLAALGLLLAAVLGDLTYNDIARYVIMVGVAFAEMNNGGDLIHLALIATFKVQPGYLLFFLLRKQWSPRESTILASAAVVLQICAAAWQSTKSMQVLNALAMGWLYIRAIVVPGALSKAMPLICMCVPGVLSLTPHAIRVSMVTIAAGTLIKGTKGTSVRKHMPYFLGLVGAVAGLDPLGMLGYSLLTYSSGKRSWPAGEIMTAVGLTCAMIGALSGNAMNDIAGPAAAASLIFVAYAISGRSADVFLEKAGEISWIDDAAVSGSSPRVDVQVTDGGDFRLRHEAEASWLKNGVMAFCLVLAGVHPLAIPVAGLIWFGFVKSGRR
>ref-NC_018705-NS2
FRGDGMEPFQLGVLLMFVATQEVLRKRWTAGMALPAMLMFLCALIFGGITYMDLVRYVILVAAAFAESNSGGDIVHLALIAAFKIQPGLLVAYSLKRKWSNQESVLVGIALSMLTLAMQDWDMTIPSLLNSGAAAWLLLRAVSEGTVSAVCIPVLGMLAPGLRIAGIDVVRIGLLIIGIMSLMRERNNAAAKKKGGLLIGLALAESGLISPLVYAGLTLALKPNMRRGWPVSEALTAVGLTFALAGGIAHFDEGNMAIPLAVGGIMLVVAVVTGFTTDLWLEKAGEMTWSEEAQITGSSQRYDVEIDSDGNMRLLNDPGIPFSVWALRTGLILLSAYNPYLLPVTLGAYWLTVKAPKR
>ref-NC_024805-NS2
GLVAGLDNFSLGFLVLMLATQEGLKRRWTSKHVLLTSIALLLAMIVGDITYMDLGRYMITLGAMFAEMNSGGDVMHLALVATFRVKPAYLLGFLWRNTWSPRESMLLTCGAILMQMGMGVLACHHLMDYIHALALGWLFVRAIVVRGMASKAMPLICCMVPLTATIVANATRAGVITLAIGTMVVGQKGKAVKKAMPYLASLVGAWAGLNPLYMMVIAGLMIRNGRRSWPAGEIMSAVGLTCAMVGAISGASTNELAGPAAAAALIFTAYAISGRANDIYIEKAGEISWNTEAQVSGSSPRVDVKVTEGGDFRLRHETEDSWLKTGIIGLCLVIAGIHPFAIPVSGLLWYGLVARTAKR
>ref-NC_024806-NS2
GTFQGIDDFSLGLLILIIFIQEGLKRRMTSKYILLASLGLLLAAVLGDLTYQDIIRYVIMVGTAFAEMNTGGDLVHLALIATFKIQPGYLMPFLLRXIWSPRESTLLASAAVVLQIGAVAWQSTKSMQVLNALALGWLYIRAIVVPGAISKAMPLICMCVPGVMALTPNAIRISMLTIAAGTMIKGNKGTSVRKHVPYFLGLVGATIGLDPIGMLGYSLLTYTSGKRSWPAGEIMTAVGLTCAMVGALSGNAMNDIAGPAAAASLIFVAYAISGRSSDVFLEKAGEITWCEEAAVSGSSPRVDVQVTDGGDFRLRHEAETSWLKNGVMAFCLVLAGVHPLAIPVSGLIWFGYVKSGRR
>ref-NC_026623-NS2
YNGAMIDPFQLGLLVVFLATQEVMRKRWTARLTIPAILMALAILLFGGLTYTDVVRYLILVAAAFAEANSGGDVVHLAMIAAFKIQPVFLAVSFLRETWSNQENLMMMISAAFFQMAATDLQIEVPTVLNALAMAWMTLRALSNTKVSTIVPPLLALLSPAMRTAYLDTYRIILIMIGAMILLKERKASSAKKKGAPLLCLALASTGLFNPLTLMAGLLALDPSRKRSWPASEVFTAIGITFALVGGILECEPHTMAVPMVIAGIMGTAYVISGRHTDMWLEKAADISWELDAEVTGSSPRLDVTLDDDGNFNLIDDPGTPWKLWMMRMACLTVGAFSPWAILPSLFAYWMTIKFTKRS
>ref-NC_027700-NS2
SKGDVDPFSLGLLMLFLCSDMFLMKRFSMRALIVGSVVMLGAMTLGSLTYLDLFRYIITVGMYMAEVNSGGDVTHLALIAVFRARAGFVSVLALKQMWSPRERFVAACGIIMVQIALGDIINTSIMEWLNAAGMSILIIKSIVEPKRCNAVLPLLCFLTPLTLVELQRAAMLTCSMVILVTVWQTDSVSTRKSIPLVALTICSFFKWTSPFLGLVCYLAFTRMPQRSWPLGETMAAVGLVGVLAGMGLKDMNGMLGPVAVGGVLLIIMSLSGKVDGLVIRKISEISWDEEAEISGASHRYDVEQTDTGEFKLRNEEPAPWIQVFVLTVAIVSAAIHPACLAVVTLAWFLWQKTSTR
>ref-NC_027709-NS2
DNGELLSEGGIPGIVALFVVLEYIIRRRPSTGTTVVWGGVIVLALLVTGMVRIEGLVRYVVAVGIAFHLELGPEIVALMLLQAVFELRVGLLSAFALRRGLTVREMVTTYFLLLVLEMGLPSVSFEDLWKWSDALAMGALIFRACSAEGKTGTGLLIIALMTQQDVVTIHHGLVCFLAVASACSVWRLLRGHKEQKGLTWIVPLARLLGGEGSGIRLLAFWELAAHRRKRSFSEPLTVVGVMLTLASGMMRHTSQEALCALAVASFFLLMLVLGTRRMQLVAEWSGCVEWHPELVNEGGEISLRVRQDSMGNFHLTELEKEERMMAFWLLAGLVASAFHWSGILGVMGLWTLTEMMRSSRR
>ref-NC_027817-NS2
YADFRLPKLPNSRPSAMVGSLVNLLCLMFSIQIVTKTMRARTLMRFYLCCLVFMFFGMPTLFGLSGFLAWMMILPISHNSVTMCNLTVHLWAVLLNQSSAMFLWGLTLRSQIQRSTAGQMLLFTMQMLHHAIYAHSWVFGWVIEVCLSVGLMMNLLTVIDTVHPKLIAYLLFFGWKTGMCVVCAWLLIYSIRRWNSIVAAAPAAGGWRSGYRTMMSSSLIVIFISVGIAGVIASDYGGYPAAAAVTAALLIMGIKMFDFLTTRLSLEFVSAGMFPEGVEKAFEPDSVSDLFRASFTVDGIKLQDHVEPIPILFAILYIFIGAVACKVNPALGIVYAIAMFATPLPELMRLYMMTIYSSTFR
>ref-NC_027819-NS2
NQDFRLGRVDHISRMLVLSIALMCITARTRRKWLFRAVGTWLAFLLIGMPLLSDWQSWGWLILSQGIAGHKGYTMWMCHLWMAIHTGTGHIWFLAMMWRRQMWAPLDVKCFVLILQWAYAIIAHKLGQFGSLLDAGLMIAAGMGLSTAAANMGFHEWLISSTLLVASWQTAIFSVAIILAVITVRALKVSYQAHSSAWRNGLRSATTLCGLLRALQRNTRMLLLNGPWLVLGCLLVLSRDWSLAANGLMTQRYVPTFAMTCLAFLARPLSCCVRGSIDWIYQCKFVAGSGIVIAAVWLLEEAQRPELAMAILVLATAVWFYFSMTAKVSLELVQIPGDSCTIGETKQFSPERRLTGTRGHHGVEVHSEQEKNGVIMNICVGVVLLLVFCFNWQVGIVGALWYVFSGSNKFIPALANAVFRWNLR
>ref-NC_029055-NS2
GSTDHMDHFSLGLVVVMLMVQEGMKKRMTSKAIITSAAFLLAVMIVGGFTYQDFGRLVVLVGAAFAEMNTGGDVAHLALVAAFKVRPAMLVSFMFRALWTPRESLLLALAACLLQVSVTPLDHSIMIVVDGIALSWLCLKAILVPRTPNIALPLLAMLSPMLQGTTIVAWRAMMAALAVITLASMKHGRGVKKTFPYTIGCILGSMGLVENLGLVGLLLLTASKKRSWPPSEVMTAVGLICAIVGGLTKTDIDMAGPMAAIGLLVVSYVVSGKSVDMYIEKVCDISWDKDAEITGTSPRLDVALDDSGDFSLIQDDGPPTREIVLKVFLMCVCGVSPIAIPFAAAAWFVYIKSGKR
>ref-NC_030400-NS2
EKLQQFFRESPPRSASNAFLVGILLHLLTARTRHRWVARCVGTWIVFLTFGHPQASSVSSWLWLVLSSSLAYVPGGTSLVVHFYLALRLSSAHLFYIGWLLRSGTLSTEIARIAHLLVQMCSRVLYSWNLNLSWVDHMIFPLYAASVLVAHQQIKQFSEIALQGSIIGVHLFQYPFEGGLTLLFGSCFIWLLPSIKGWFTSPTLWASGLRKPRPHLITCVYFTSIYVTATWMESMGLQNTGLLVVMGGISLWILTQLLPPNTLELERLHGQDLPDGCEEEISTPLPRELSGKYATDGVELIGHTDASTVPANLFVLVGCAGIMAMNLYVGMFLTVFALATDAAIWIPRLIAGEFAQR
>ref-NC_030401-NS2
TNAEDFLNPKLILTKANAMVGEVVNLLCLALAMQVVTHTWKHKSLARFHLCCLLFLLFGVPTVFGFVGIYTWMNILPISHGSARMCNLTIHLWAVLQHRSSSMFLWGQTLRSQFQTSLAGQMLLLTMQMLHQAIYAHSSMLGWGIEVFLSVIVMQNLYTVVDHIHPRLIAYCLLFGWRTGLCIGCGFLLTFLMKRWMTIVAASPSAGGWRSGYRALCSSTLTLLFTAVGVAGVIASDYGGYPGASAAIAACLIAGIKMTDFLATRLSLEFVSTGQFPEGTTIEKEKETYDGTFRASFTVEGIKLLDHTEPVPLVFAISYVALGAVTCKIHPGLGVIYAVAMVATNLPSLLQVYVLSICTNAFR
>ref-NC_004102-NS2
LDTEVAASCGGVVLVGLMALTLSPYYKRYISWCMWWLQYFLTRVEAQLHVWVPPLNVRGGRDAVILLMCVVHPTLVFDITKLLLAIFGPLWILQASLLKVPYFVRVQGLLRICALARKIAGGHYVQMAIIKLGALTGTYVYNHLTPLRDWAHNGLRDLAVAVEPVVFSRMETKLITWGADTAACGDIINGLPVSARRGQEILLGPADGMVSKGWRLL
>ref-NC_009823-NS2
YDASVHGQIGAALLVMITLFTLTPGYKTLLSRFLWWLCYLLTLGEAMVQEWAPPMQVRGGRDGIIWAVAIFYPGVVFDITKWLLAVLGPAYLLKGALTRVPYFVRAHALLRMCTMARHLAGGRYVQMALLALGRWTGTYIYDHLTPMSDWAASGLRDLAVAVEPIIFSPMEKKVIVWGAETAACGDILHGLPVSARLGREVLLGPADGYTSKGWSLL
>ref-NC_009824-NS2
WSGEDSATLGAGVLVLFGFFTLSPWYKHWIGRLMWWNQYTICRCESALHVWVPPLLARGSRDGVILLTSLLYPSLIFDITKLLMAVLGPLYLIQATITTTPYFVRAHVLVRLCMLVRSVIGGKYFQMIILSIGRWFNTYLYDHLAPMQHWAAAGLKDLAVATEPVIFSPMEIKVITWGADTAACGDILCGLPVSARLGREVLLGPADDYREMGWRLL
>ref-NC_009825-NS2
YDQEVAGSLGGAIVVMLTILTLSPHYKLWLARGLWWIQYFIARTEAVLHVYIPSFNVRGPRDSVIVLAVLVCPDLVFDITKYLLAILGPLHILQASLLRIPYFVRAQALVKICSLLRGVVYGKYFQMVVLKSRGLTGTYIYDHLTPMSDWPPYGLRDLAVALEPVVFTPMEKKVIVWGADTAACGDIIRGLPVSARLGNEILLGPADTETSKGWRLL
>ref-NC_009826-NS2
LDSSDGGTVGCLVLIVLTIFTLTPGYKKKVVLVMWWLQYFIARVEAIIHVWVPPLQVKGGRDAVIMLTCLFHPALGFEITKILFGILGPLYLLQHSLTKVPYFLRARALLRLCLLAKHLVYGKYVQAALLHLGRLTGTYIYDHLAPMKDWAASGLRELTVATEPIVFSAMETKVITWGADTAACGNILAVLPVSARRGREIFLGPADDIKTSGWRLL
>ref-NC_009827-NS2
WDNSQAASLGVVALLVLTIFTLSPMYKQLLTHAIWWNQYMLARAEAMIHDWVPDLRVRGGRDAIILLTCLLHPHLGFEVTKILLAILAPLYILQHSLLKVPYFVRAHILLRACMFFRKVAAGKYVQACLLRLGAWTGTYIYDHLAPLSEWASDGLRDLAVAVEPVIFSPMEKKIITWGADTAACGDILRGLPVSARLGDLVLLGPADDMRHGGWKLL
>ref-NC_030791-NS2
LDREVSAALGTGMLAIILLVTLGPHYKRLLALILWWVTYFLTRCEAALQTWVPPLNPRGGRDGFILCVLLCYPGLVFDITKWLLVMMCPLYLLQLCLVRTPYFVRAQALIRVCSLFKTLAGGRYVQAALLTIGRWTGTYIYNHLAPLETWAAGGLRDLAVAVEPVIFSPMEKKIIVWGAETTACGDILCGLPVSARLGREVLLGPADDYRSMGWQLL
>ref-NC_024377-NS2
RTSVLGLQFCFDFTVDWQPSLLGWAAALVVSWAIFVTSTMSVGGWKHKARLYGGWCRVYQLIRYYVAASPGGTRSESRVLLGCWLGAQFLFPEECGLVVLVVMCFCGLLDGTDWAIEILLTSRPQFGRMARLLNSLIESGDRVSSTRLAEALARRGIFLYDHMGQVTRRARDQLLEWEAFLEPVSFTNRDAEIIRDAARVLTCGASVAGKPVVARRGDEVLIGTLLSLGELPPGFVPT
>ref-NC_027998-NS2
RPGRVFGLEVCADISWLVEFTGNCTWYMSCVFSFWCAVFAFTSPLGRHYKIQIYRYWAQVYARLVLAVGCGPLGREFHFRASVGVLWCGACMLWPRECSEISLVFILCALTVDTIDTWLVACLSAGPSARTLAILADDMARIGDHRALRAVLRCFGSRGTYIYNHMGQVSERVAQAVRDLGGCLEPVVLEEPTFTEIVDDTMSLVCGQLLGGKPVVARCGTRVLVGHLNPEDLPPGFQLS
>ref-NC_021154-NS2
GPERGALGYQLCLPEWDASIDLDDVWWYAAAFFTFCCISISLLTRRGVVFKLRVYARWCRLYCWLQLVVGHTPAGDYFRWRGASSLLWLAAGVLWPAEVAVVSMIIVCFAAVLDVFDALLERLLTASPSLRPLVNLANTLHSCLSDPELAAFLRARWHRGELLYDHAGQVAASLRERVVALDGCLEPLTLTGEALQEVYDDTFALTCGRWCGGNPVVARCGRSVLVGSAASVASLPPGYTL
>ref-NC_020902-NS2
RPGRSALGLEVCFDVSGPGWDVPDGILPWCFAGVVSGLLIHLATFNHWCRGWKLRLYSRWAFWYSRVISWFEVCPFGMDSEFRCVRRLWFFACLFWPDECTLVLGTYCFVAICFDLFDMFLEACLCSQPSVSSVAALLEVAARARSSSLVRWIIVRARARGVLLYQHRGDVPWRAVSFLKELECALEPVSVSAEDLEVVRDAAGVLECGKFFRGKPVVARRGDQVIVGAAKSVGALPVGFVP
