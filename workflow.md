# Main steps of the FLAVi pipeline

## Overview

The pipeline starts with parallel execution of genewise (comes with Wise v2.4.1, Birney et al., 2004) and TransDecoder v.3.0.0 (Haas et al., 2013) analyses. In TransDecoder, we use dedicated databases downloaded from UniProt (UniProt Consortium, 2018) and blastp (comes with BLAST v2.4.0+, Altschul et al., 1990) for homology-based annotation. We also employ hmmscan (comes with HMMER v3.1b2, available at http://hmmer.org/) to search the peptides for protein domains using Pfam (El-Gebali et al., 2018). If predictions from both genewise and TransDecoder match, we pool the alignments and calculated distance matrices with distmat (comes with EMBOSS v6.6.0, Rice et al., 2000). We apply the distance matrices for outlier testing using the Tukey method (Tukey, 1977) with the help of homemade R scripts. The Tukey method to search for outliers leverages on the interquartile range and is applicable to most ranges since it is not dependent on distributional assumptions. It also ignores the mean and standard deviation, making it resistant to being influenced by the extreme values in the range. Finally, we remove all sequences that were selected as outliers.

## Output files

 The user receives annotations (in the GFF3 format) and, if Biopython is available, the user also receives the concatenated alignments (in FASTA and NEXUS format) and a partition file (in NEXUS format).

The user also has access to directories for each gene that contain information useful to analyze each step of the pipeline.

## Availability

 The FLAVi pipeline is available at https://gitlab.com/MachadoDJ/FLAVi under the GNU's GPL v3.0 licence (see https://www.gnu.org/licenses/gpl-3.0.en.html).

## References

 Birney, E., Clamp, M., and Durbin, R. 2004. Genewise and genomewise. *Genome Research*, **14(5)**: 988-995.

 Haas, B. J., Papanicolaou, A., Yassour, M., Grabherr, M., Blood, P. D., Bowden, J., Couger, M. B., Eccles, D., Li, B., Lieber, M., et al. 2013. De novo transcript sequence reconstruction from rna-seq using the trinity platform for reference generation and analysis. *Nature Protocols*, **8(8)**: 1494.

 UniProt Consortium, 2018. UniProt: a worldwide hub of protein knowledge. *Nucleic Acids Research*, **47(D1)**: D506-D515.

 Altschul, S. F., Gish, W., Miller, W., Myers, E. W., and Lipman, D. J. 1990. Basic local alignment search tool. *Journal of Molecular Biology*, **215(3)**: 403-410.

 El-Gebali, S., Mistry, J., Bateman, A., Eddy, S. R., Luciani, A., Potter, S. C., Qureshi, M., Richardson, L. J., Salazar, G. A., Smart, A., et al. 2018. The Pfam protein families database in 2019. *Nucleic Acids Research*, **47(D1)**: D427-D432.

 Rice, P., Longden, I., and Bleasby, A. 2000. Emboss: the European molecular biology open software suite. *Trends in Genetics*, 16(6): 276-277.

 Tukey, J. W. 1977. E**xploratory Data Analysis, Section 2C**. *Addison-Wesley*.