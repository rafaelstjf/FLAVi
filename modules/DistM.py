import glob
import os.path
import re
import subprocess


def count_fasta_sequences(file_path):
	sequence_count = 0
	with open(file_path, 'r') as fasta_file:
		for line in fasta_file:
			if line.startswith('>'):
				sequence_count += 1
	return sequence_count


def main(peptides_dic, distmat, output, verbose):
	processes = []
	for pep_file in peptides_dic:
		pep_name = peptides_dic[pep_file]
		file_path = "{0}/{1}/{1}.align.fa".format(output, pep_name)
		if(os.path.isfile(file_path)):
			if count_fasta_sequences(file_path) >= 3:
				if(verbose):
					cmd="{1}/distmat -sequence {2}/{0}/{0}.align.fa -nucmethod 0 -gapweight 0.2 -outfile {2}/{0}/{0}.dist.tsv".format(pep_name, distmat, output)
				else:
					cmd="{1}/distmat -sequence {2}/{0}/{0}.align.fa -nucmethod 0 -gapweight 0.2 -outfile {2}/{0}/{0}.dist.tsv > /dev/null 2>&1".format(pep_name, distmat, output)
				p=subprocess.Popen(cmd, shell=True)
				processes.append(p)

	for p in processes:
		p.wait()
	for pep_file in peptides_dic:
		pep_name = peptides_dic[pep_file]
		if(os.path.isfile("{0}/{1}/{1}.dist.tsv".format(output, pep_name))):
			handle=open("{0}/{1}/{1}.dist.tsv".format(output, pep_name),"r")
			table=[line for line in handle.readlines() if re.match("^\t.+", line)]
			handle.close()
			if(table):
				matrix={}
				names=[]
				l=0
				for line in table[1:]:
					r=0
					line=[i.strip() for i in line.rstrip().split("\t")[1:]]
					for i in line[ : -2]:
						matrix["{}:{}".format(l, r)]=i
						r+=1
					l+=1
					names.append(re.compile("(.+)\s\d+").findall(line[-1])[0])
				for cell in matrix:
					if(not matrix[cell]):
						l,r=cell.split(":")
						matrix[cell]=matrix["{}:{}".format(r,l)]
				csv=",{}\n".format(",".join(names))
				for l in range(0,len(names)):
					csv+="{},".format(names[l])
					for r in range(0,len(names)):
						if(l==r):
							csv+=","
						else:
							csv+="{},".format(matrix["{}:{}".format(l, r)])
					csv="{}\n".format(csv[:-1])
				handle=open("{0}/{1}/{1}.dist.csv".format(output, pep_name),"w")
				handle.write(csv)
				handle.close()
	return
