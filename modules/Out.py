import os
import re
import subprocess

def main(rscript, find_outliers, peptides_dic, output, verbose):
	processes = []
	for pep_file in peptides_dic:
		pep_name = peptides_dic[pep_file]
		if(os.path.isfile("{0}/{1}/{1}.dist.csv".format(output, pep_name))):
			if(verbose):
				cmd="{0}/Rscript {1}/find_outliers.R {3}/{2}/{2}.dist.csv".format(rscript, find_outliers, pep_name,output)
			else:
				cmd="{0}/Rscript {1}/find_outliers.R {3}/{2}/{2}.dist.csv > /dev/null 2>&1".format(rscript, find_outliers, pep_name, output)
			p=subprocess.Popen(cmd,shell=True)
			processes.append(p)
	for p in processes:
		p.wait()
	for pep_file in peptides_dic:
		pep_name = peptides_dic[pep_file]
		if(os.path.isfile("{0}/{1}/{1}.dist.csv.outliers".format(output, pep_name))):
			remove(pep_file, pep_name, output, verbose)
	return

def remove(p, pep_name, output, verbose):
	handle = open("{0}/{1}/{1}.dist.csv.outliers".format(output, pep_name), "r")
	outliers = [l.strip() for l in handle.readlines()]
	outliers = list(filter(None, outliers))
	handle.close()
	verified_outliers = []
	if(outliers):
		handle = open(p, "r")
		ref = ";".join([l.strip()[1:] for l in handle.readlines() if re.match(">", l)])
		handle.close()
		for i in outliers:
			if(not re.match("{}".format(i), ref)):
				verified_outliers.append(i)
		outliers = []
		ref = ""
	if(verified_outliers):
		handle = open("{0}/{1}/{1}.align.fa".format(output, pep_name), "r")
		entries = re.compile("(>[^>]+)", re.M|re.S).findall(handle.read())
		handle.close()
		filtered = []
		for i in entries:
			if(not re.compile(">([^\n]+)").findall(i)[0] in verified_outliers):
				filtered.append(i)
		handle = open("{0}/{1}/{1}.final.fasta".format(output, pep_name), "w")
		handle.write("\n".join(filtered))
		handle.close()
	else:
		handle = open("{0}/{1}/{1}.align.fa".format(output, pep_name), "r")
		entries = handle.read()
		handle.close()
		handle = open("{0}/{1}/{1}.final.fasta".format(output, pep_name), "w")
		handle.write(entries)
		handle.close()
	os.remove("{0}/{1}/{1}.align.fa".format(output, pep_name))
	trailing_gaps(output, pep_name)
	return

def trailing_gaps(output, pep_name):
	alignment = read_fasta(output, pep_name) # Execute function read_fasta, which will return variable alignment
	handle_trailing_gaps(alignment) # Execute function handle_trailing_gaps, giving the alignment variable to it
	report(output, pep_name, alignment) # Execute function report, giving the alignment variable to it
	return

def read_fasta(output, pep_name): # Reads the input file and parses its contents into a dictionary
	alignment = {} # Defines an empty dictionary called alignment. Dictionaries look like: dic = {word1 : description1, workd2 : description2 ...}.
	handle = open("{0}/{1}/{1}.final.fasta".format(output, pep_name),"r") # Open the input in read mode
	for header, sequence in re.compile("(>[^\n\r]+)([^>]+)", re.M | re.S).findall(handle.read()):
		header = header.strip() # Removes invisible characters from each end of header
		sequence = re.sub("\s", "", sequence) # Removes invisible characters the sequence
		alignment[header] = sequence.strip() # Add the entry header to the alignment dictionary and associate it with the respective sequence
	handle.close() # Close the input file
	return alignment # Finish function and return the alignment dictionary

def handle_trailing_gaps(alignment): # Edit the sequences in the alignment
	for header in alignment: # For each term in the dictionary
		sequence = alignment[header] # Get the sequence
		gap = "-" # Defines the gap symbol
		mis = "?"
		try: # Tries to find gaps in the beginning of the sequence
			left = len(re.compile("^{}+".format(gap)).findall(sequence)[0])
		except: # Exception if there are no gaps in the beginning of the sequence
			left = 0
		try: # Tries to find gaps in the end of the sequence
			right = len(re.compile("{}+$".format(gap)).findall(sequence)[0])
		except: # Exception if there are no gaps in the end of the sequence
			right = 0
		sequence = sequence[left:len(sequence)-right] # Takes the part of the sequence between the trailing gaps
		sequence = "{}{}{}".format(left*mis,sequence,right*mis) # Edits the sequences, replacing the trailing gaps
		alignment[header] = sequence # Updates the dictionary
	return alignment # Returns the edited dictionary

def report(output, pep_name, alignment):
	output = "{0}/{1}/{1}.final.fasta".format(output, pep_name)
	handle = open(output, "w")
	for head in alignment: # For each item in the dictionary
		handle.write("{}\n{}\n".format(head,alignment[head])) # Print the corresponding header and sequence into the STDOUT
	handle.close()
	return
