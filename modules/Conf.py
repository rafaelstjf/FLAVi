import re

def main(configuration_file):
	handle=open(configuration_file,"r")
	configuration=handle.read()
	handle.close()
	configuration=re.sub("#.*","",configuration)
	configuration=re.sub("\n+","\n",configuration)
	arguments={}
	try:
		arguments["genomes"]=re.compile(">genomes=([^>]+)",re.I|re.M|re.S).findall(configuration)[0].strip()
	except:
		sys.stderr.write("! ERROR reading configuration file (check '>genomes')\n")
		exit()
	try:
		arguments["peptides"]=[i.strip() for i in re.compile(">peptides=([^>]+)",re.I|re.M|re.S).findall(configuration)[0].split(",")]
	except:
		sys.stderr.write("! ERROR reading configuration file (check '>peptides')\n")
		exit()
	try:
		arguments["genewise"]="{}/genewise".format(re.compile(">genewise=([^>]+)",re.I|re.M|re.S).findall(configuration)[0].strip())
	except:
		sys.stderr.write("! ERROR reading configuration file (check '>genewise')\n")
		exit()
	try:
		arguments["genewise_handler"]="{}/genewise_handler.py".format(re.compile(">genewise_handler=([^>]+)",re.I|re.M|re.S).findall(configuration)[0].strip())
	except:
		sys.stderr.write("! ERROR reading configuration file (check '>genewise_handler')\n")
		exit()
	try:
		arguments["blastp"]="{}/blastp".format(re.compile(">blastp=([^>]+)",re.I|re.M|re.S).findall(configuration)[0].strip())
	except:
		sys.stderr.write("! ERROR reading configuration file (check '>blastp')\n")
		exit()
	try:
		arguments["hmmscan"]="{}/hmmscan".format(re.compile(">hmmscan=([^>]+)",re.I|re.M|re.S).findall(configuration)[0].strip())
	except:
		sys.stderr.write("! ERROR reading configuration file (check '>hmmscan')\n")
		exit()
	try:
		arguments["transdecoder"]=re.compile(">transdecoder=([^>]+)",re.I|re.M|re.S).findall(configuration)[0].strip()
	except:
		sys.stderr.write("! ERROR reading configuration file (check '>transdecoder')\n")
		exit()
	try:
		arguments["protdb"]=re.compile(">protdb=([^>]+)",re.I|re.M|re.S).findall(configuration)[0].strip()
	except:
		sys.stderr.write("! ERROR reading configuration file (check '>protdb')\n")
		exit()
	try:
		arguments["pfam"]=re.compile(">pfam=([^>]+)",re.I|re.M|re.S).findall(configuration)[0].strip()
	except:
		sys.stderr.write("! ERROR reading configuration file (check '>pfam')\n")
		exit()
	try:
		arguments["translatorx"]=re.compile(">translatorx=([^>]+)",re.I|re.M|re.S).findall(configuration)[0].strip()
	except:
		sys.stderr.write("! ERROR reading configuration file (check '>translatorx')\n")
		exit()
	try:
		arguments["rscript"]="{}".format(re.compile(">rscript=([^>]+)",re.I|re.M|re.S).findall(configuration)[0].strip())
	except:
		sys.stderr.write("! ERROR reading configuration file (check '>rscript')\n")
		exit()
	try:
		arguments["find_outliers"]="{}".format(re.compile(">find_outliers=([^>]+)",re.I|re.M|re.S).findall(configuration)[0].strip())
	except:
		sys.stderr.write("! ERROR reading configuration file (check '>find_outliers')\n")
		exit()
	try:
		arguments["distmat"]="{}".format(re.compile(">distmat=([^>]+)",re.I|re.M|re.S).findall(configuration)[0].strip())
	except:
		sys.stderr.write("! ERROR reading configuration file (check '>distmat')\n")
		exit()
	return arguments
