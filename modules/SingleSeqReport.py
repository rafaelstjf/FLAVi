import glob
import os
import re
import sys

def main(peptides, output):
	return_dic = {}
	for pep_file in peptides:
		pep_name = re.compile("([^\.\/]+)\..+").findall(pep_file)[0]
		return_dic = minion(output, pep_name, return_dic)
	minion(output, pep_name, return_dic)
	return

def minion(output, pep_name, return_dic):
	cwd = os.getcwd()
	annotations = ""
	seqs = {}
	for file in glob.glob("{0}/{1}/*.gw.gff".format(output, pep_name)):
		print(file)
	return

# 			handle = open(file, "r")
# 			data = handle.read()
# 			handle.close()
# 			entries = re.compile("^>([^\n\r]+)([^>]+)",re.MULTILINE|re.DOTALL).findall(data)
# 			for head, seq in entries:
# 				head = head.strip()
# 				seq = re.sub(r"[\s\n\r-]", "", seq)
# 				seqs[seq] = head
# 		for fasta in glob.glob("*.gw.fasta"):
# 			handle = open(fasta, "r")
# 			data = handle.read()
# 			handle.close()
# 			seq = re.compile("^>[^\n\r]+([^>]+)",re.MULTILINE|re.DOTALL).findall(data)[0]
# 			seq = re.sub(r"[\s\n\r-]", "", seq)
# 			if (seq in seqs):
# 				gff = "{}.gff".format(re.compile("(.+)\.fasta$").findall(fasta)[0])
# 				handle = open(gff, "r")
# 				data = handle.read()
# 				handle.close()
# 				data = re.sub(r"\s*\n+\s*", "\n", data.strip())
# 				data = re.sub("\n", ";source={};name={}\n".format(seqs[seq], pep_name), data)
# 				annotations += "{}\n".format(data)
# 		os.chdir(cwd)
# 		return_dict[pep_name] = annotations
# 	return annotations
#
# def report(output, return_dict):
# 	delete = []
# 	for key in return_dict:
# 		if (return_dict[key] == ''):
# 			delete.append(key)
# 	for key in delete:
# 		del return_dict[key]
# 	if (return_dict):
# 		handle = open("{0}/output.gff".format(output), "w")
# 		for pep_name in return_dict:
# 			handle.write(return_dict[pep_name])
# 		handle.close()
# 	return
