import glob
import os
import re
import subprocess
import sys

def main(gh, gw, genome, selected, peptides_dic, output, verbose):
	processes = []
	for pep_file in peptides_dic:
		pep_name = peptides_dic[pep_file]
		if(os.path.isdir("{0}/{1}".format(output, pep_name))):
			sys.stderr.write("ERROR: directory {0}/{1} already exists.\n".format(output, pep_name))
			exit()
		else:
			if(verbose):
				cmd="mkdir {4}/{5} ; python3 {0} -g {1} -i {2} -p {3} -o {4}/{5}".format(gh, gw, genome, pep_file, output, pep_name)
			else:
				cmd="mkdir {4}/{5} ; python3 {0} -g {1} -i {2} -p {3} -o {4}/{5} > /dev/null 2>&1".format(gh, gw, genome, pep_file, output, pep_name)
			p = subprocess.Popen(cmd, shell=True)
			processes.append(p)
	for p in processes:
		p.wait()
	clean(output)
	return

def clean(output):
	for suffix in ["AA", "err", "out"]:
		for file in [file for file in glob.glob("{0}/*/*{1}".format(output, suffix))]:
			os.remove(file)
	return
