import os
import re
import sys

def main(file):
	test = True
	nseqs = 0
	if (not os.path.isfile(file)):
		sys.stderr.write("! WARNING: Could not finf the file {}.\n".format(file))
		test = False
	elif (os.stat(file).st_size >= 1000000000):
		sys.stderr.write("! WARNING: file {} is too big ({} bytes).\n".format(file, os.stat(file).st_size))
		test = False
	else:
		try:
			handle = open(file, "r")
			data = handle.read()
			handle.close()
			entries = re.compile("^(>[^\n\r]+)[^>]+",re.MULTILINE|re.DOTALL).findall(data)
			nseqs = len(entries)
			if(nseqs==0):
				sys.stderr.write("! WARNING: The file {} does not appear to be in FASTA format.\n".format(file, nseqs))
				test = False
		except:
			sys.stderr.write("! WARNING: Could not open the file {} to read.\n".format(file, statinfo.st_size))
			test = False
	return test, nseqs
