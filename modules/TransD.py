import glob
import os
import re
import shutil
import subprocess
import sys

def main(td, blastp, protdb, hmmscan, pfam, peptides_dic, processors, output, verbose):
    cwd = os.getcwd() # Path to FLAVI_HOME
    processes = []
    for pep_file in peptides_dic:
        pep_name = peptides_dic[pep_file]
        if (os.path.isdir("{0}/{1}".format(output, pep_name))):
            os.chdir("{0}/{1}".format(output, pep_name))
            for cdna in [file for file in glob.glob("*.gw.fasta")]:
                if(verbose):
                    cmd="cd {3}/{2} ; {0}/TransDecoder.LongOrfs -t {1} -m 10".format(td, cdna, pep_name, output)
                else:
                    cmd="cd {3}/{2} ; {0}/TransDecoder.LongOrfs -t {1} -m 10 > /dev/null 2>&1".format(td, cdna, pep_name, output)
                p=subprocess.Popen(cmd, shell=True)
                processes.append(p)

    for p in processes:
        p.wait()

    os.chdir(cwd)
    processes = []
    for pep_file in peptides_dic:
        pep_name = peptides_dic[pep_file]
        if (os.path.isdir("{0}/{1}".format(output, pep_name))):
            os.chdir("{0}/{1}".format(output, pep_name))
            for cdna in [file for file in glob.glob("*.gw.fasta")]:
                if(verbose):
                    cmd = "{0} -query {1}.transdecoder_dir/longest_orfs.pep -db {2} -max_target_seqs 1 -outfmt 6 -evalue 1e-5 -num_threads {3} > {1}.outfmt6".format(blastp, cdna, protdb, processors)
                else:
                    cmd = "{0} -query {1}.transdecoder_dir/longest_orfs.pep -db {2} -max_target_seqs 1 -outfmt 6 -evalue 1e-5 -num_threads {3} > {1}.outfmt6 2> /dev/null".format(blastp, cdna, protdb, processors)
                p = subprocess.Popen(cmd, shell=True)
                p.wait()
                if(verbose):
                    cmd = "{0} --cpu {1} --domtblout {2}.pfam.out {3} {2}.transdecoder_dir/longest_orfs.pep".format(hmmscan, processors, cdna, pfam)
                else:
                    cmd = "{0} --cpu {1} --domtblout {2}.pfam.out {3} {2}.transdecoder_dir/longest_orfs.pep > /dev/null 2>&1".format(hmmscan, processors, cdna, pfam)
                p = subprocess.Popen(cmd, shell=True)
                p.wait()

    os.chdir(cwd)
    processes = []
    for pep_file in peptides_dic:
        pep_name = peptides_dic[pep_file]
        path = "{0}/{1}".format(output, pep_name)
        os.chdir(path)
        if (os.path.isdir(path)):
            for cdna in [file for file in glob.glob("*.gw.fasta")]:
                if(verbose):
                    cmd = "cd {0} ; {1}/TransDecoder.Predict -t {2} --retain_pfam_hits {2}.pfam.out --retain_blastp_hits {2}.outfmt6 --no_refine_starts --single_best_only".format(path ,td, cdna)
                else:
                    cmd = "cd {0} ; {1}/TransDecoder.Predict -t {2} --retain_pfam_hits {2}.pfam.out --retain_blastp_hits {2}.outfmt6 --no_refine_starts --single_best_only > /dev/null 2>&1".format(path, td, cdna)
                p = subprocess.Popen(cmd,shell=True)
                processes.append(p)

    for p in processes:
        p.wait()

    for pep_file in peptides_dic:
        pep_name = peptides_dic[pep_file]
        path = "{0}/{1}".format(output, pep_name)
        os.chdir(path)
        if (os.path.isdir(path)):
            for cdna in [file for file in glob.glob("*.gw.fasta")]:
                print(f"{cdna} in {path}")
                handle=open(cdna,"r")
                g=re.compile(">[^\n]+\n([^>]+)",re.M|re.S).findall(handle.read())[0].strip()
                handle.close()
                try:
                    handle=open("{}.transdecoder.cds".format(cdna), "r")
                except:
                    sys.stderr.write("! WARNING: no results from TransDecoder.Predict (file {}.transdecoder.cds not found).\n".format(cdna))
                    cmd = "rm -r {0}*".format(cdna)
                    p=subprocess.Popen(cmd, shell=True)
                    p.wait()
                else:
                    trasdecoder_cds_data = handle.read().strip()
                    if len(trasdecoder_cds_data) == 0:
                        sys.stderr.write("! WARNING: no results from TransDecoder.Predict (file {}.transdecoder.cds exists, but the file is empty).\n".format(cdna))
                        cmd = "rm -r {0}*".format(cdna)
                        p=subprocess.Popen(cmd, shell=True)
                        p.wait()
                    else:
                        t=re.compile(">[^\n]+\n([^>]+)",re.M|re.S).findall(trasdecoder_cds_data)[0].strip()
                        handle.close()
                        if(g!=t):
                            sys.stderr.write("! WARNING: {} failed TransDecoder test\n".format(cdna))
                            cmd = "rm -r {0}*".format(cdna)
                            p=subprocess.Popen(cmd, shell=True)
                            p.wait()
    os.chdir(cwd)
    clean(output)
    return

def clean(output):
    for suffix in ["cmds", "pfam.out", "outfmt6", "FA"]:
        for file in [file for file in glob.glob("{0}/*/*{1}".format(output, suffix))]:
            os.remove(file)
    for dir in [dir for dir in glob.glob("{0}/*/*.transdecoder_dir*".format(output))]:
        shutil.rmtree(dir, ignore_errors=True)
    return
