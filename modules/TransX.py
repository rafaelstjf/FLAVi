import fileinput
import glob
import os
import re
import subprocess
import sys

def main(peptides_dic, translatorx, output, verbose):
	cwd = os.getcwd() # Path to FLAVI_HOME
	processes = []
	for pep_file in peptides_dic:
		pep_name = peptides_dic[pep_file]
		gw_fasta_files = [file for file in glob.glob("{0}/{1}/*.gw.fasta".format(output, pep_name))]
		if (len(gw_fasta_files) >= 1):
			cmd = "cat {0}/{1}/*.gw.fasta > {0}/{1}/{1}.cdna.fa".format(output, pep_name)
			p = subprocess.Popen(cmd, shell = True)
			processes.append(p)
	for p in processes:
		p.wait()
	processes = []
	for pep_file in peptides_dic:
		pep_name = peptides_dic[pep_file]
		gw_fasta_files = [file for file in glob.glob("{0}/{1}/*.gw.fasta".format(output, pep_name))]
		if (len(gw_fasta_files) >= 2):
			if(verbose):
				cmd = "{0} -i {2}/{1}/{1}.cdna.fa -o {2}/{1}/{1}_align -p F -c 1 ; mv {2}/{1}/{1}_align.nt_ali.fasta {2}/{1}/{1}.align.fa".format(translatorx, pep_name, output)
			else:
				cmd = "{0} -i {2}/{1}/{1}.cdna.fa -o {2}/{1}/{1}_align -p F -c 1  > /dev/null 2>&1 ; mv {2}/{1}/{1}_align.nt_ali.fasta {2}/{1}/{1}.align.fa".format(translatorx, pep_name, output)
			p = subprocess.Popen(cmd, shell = True)
			processes.append(p)
		else:
			cmd = "cp {0}/{1}/{1}.cdna.fa {0}/{1}/{1}.align.fa".format(output, pep_name)
			p = subprocess.Popen(cmd, shell = True)
			processes.append(p)
	for p in processes:
		p.wait()
	processes = []
	for pep_file in peptides_dic:
		pep_name = peptides_dic[pep_file]
		try:
			with fileinput.FileInput("{0}/{1}/{1}.align.fa".format(output, pep_name), inplace = True, backup='.bak') as file:
				for line in file:
						sys.stdout.write(re.sub(r"\..*", "", line))
		except:
			sys.stderr.write("! WARNING: check {0}/{1}/{1}.align.fa.\n".format(output, pep_name))
			pass
		clean(output, pep_name)
	os.chdir(cwd)
	return

def clean(output, pep_name):
	cmd = "for i in {0}/{1}/translatorx* {0}/{1}/{1}_align.* {0}/{1}/*.cdna.fa {0}/{1}/*.bak ; do if [ -f $i ] ; then rm $i ; fi ; done".format(output, pep_name)
	p = subprocess.Popen(cmd, shell = True)
	p.wait()
	return
