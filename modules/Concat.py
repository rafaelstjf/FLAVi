from Bio import SeqIO
import fileinput
import glob
import os
import re
import sys

def main(peptides, output):
	files = []
	for pep_file in peptides:
		pep_name = re.compile("([^\.\/]+)\..+").findall(pep_file)[0]
		files += [file for file in glob.glob("{0}/{1}/{1}.final.fasta".format(output, pep_name))]
	if(files):
		all_terminal_names = get_all_terminal_names(files) # Execute function to get all terminal names, in case some files have different terminals than the other
		all_alignments_lengths = get_all_alignments_lengths(files) # Execute function to get the length of the alignment in each file
		concatenated_sequences = concatenate_the_alignment(all_terminal_names, all_alignments_lengths) # Function to concatenate the alignments given all the terminal names and all the alignment lengths
		report_align(output, concatenated_sequences) # Report results
		report_partitions(output, all_alignments_lengths) # Report results
	return

def get_all_terminal_names(files): # Get the names of the terminals in every alignment file
	all_terminal_names = [] # Creates am empty list variable to hold the terminal names
	for file in files: # For every file in the input
		for record in SeqIO.parse(file, "fasta"): # For every sequence in that file
			if not record.id in all_terminal_names: # Check if that terminal is already on the list
				all_terminal_names += [record.id] # Append a new terminal to the list
	return all_terminal_names # Return list with all terminals

def get_all_alignments_lengths(files): # Gets the alignment length of every alignment
	all_alignments_lengths = {} # Creates an empty dictionary
	for file in files: # For every file in the input
		for record in SeqIO.parse(file, "fasta"): # For every sequence in the file
			all_alignments_lengths[file] = len(record.seq) # Associate the file with the alignment length
			break # I can break here because I only need to read the first sequence in each file, since they are all the same length
	return all_alignments_lengths # Return dictionary with the name of the file and its alignment length

def concatenate_the_alignment(all_terminal_names, all_alignments_lengths):
	concatenated_sequences = {} # Empty dictionary to hold the results
	# Help sorting the partitions
	desired_order = ['C', 'pr', 'M', 'E', 'NS1', 'NS2', 'NS3', 'NS4A', 'NS4B', 'NS5']
	ordered_files = []
	for gene in desired_order:
		for file in all_alignments_lengths:
			if f"{gene}.final.fasta" in file:
				ordered_files.append(file)
				break
			else:
				pass
	# Iterate over ordered alignment files
	for file in ordered_files: # Iterate over every entry in the dictionary
		used = [] # Keep track of the sequences that have been delt with
		for record in SeqIO.parse(file, "fasta"): # Use BioPython to head the sequences
			used += [record.id] # Append id so you know you already used it for the current file
			if not record.id in concatenated_sequences: # For new sequences
				concatenated_sequences[record.id] = record.seq # Add new
			else: # For things that are already there
				concatenated_sequences[record.id] += record.seq # Append
		for id in all_terminal_names: # For loop before closing the file, to add missing sequeces
			if not id in used: # Only if you have not used it in this file
				if not id in concatenated_sequences: # For new sequences
					concatenated_sequences[id] = "{}".format("?" * all_alignments_lengths[file]) # Add new
				else: # Or
					concatenated_sequences[id] += "{}".format("?" * all_alignments_lengths[file]) # Append
	return concatenated_sequences # Return concatenated sequences

def report_align(output, concatenated_sequences): # This function will print the concatenated sequence into your screen
	output_fasta = "{}/output.fasta".format(output)
	handle = open(output_fasta, "w")
	for id in concatenated_sequences: # For each entry on this dictionary
		handle.write(">{}\n{}\n".format(id, concatenated_sequences[id])) # Print the item and its meaning on the dictionary, using the FATSA format
	handle.close()
	convert2nexus(output_fasta)
	return # End function and returns nothing

def convert2nexus(fasta_file):
	base = os.path.splitext(fasta_file)[0]
	nexus_file = f"{base}.nex"
	sequences = []
	with open(fasta_file, "r") as handle:
		for record in SeqIO.parse(handle, "fasta"):
			sequences.append(record)
	with open(nexus_file, "w") as output_handle:
		output_handle.write("#NEXUS\n\nBEGIN DATA;\n")
		output_handle.write("DIMENSIONS NTAX={} NCHAR={};\n".format(len(sequences), len(sequences[0])))
		output_handle.write("FORMAT DATATYPE=DNA MISSING=? GAP=-;\n")
		output_handle.write("MATRIX\n")	
		for sequence in sequences:
			output_handle.write("{} {}\n".format(sequence.id, str(sequence.seq).upper()))
		output_handle.write(";\nEND;\n")
	return

def report_partitions(output, dic):
	partitions = "#NEXUS\n\nbegin sets;\n"
	position = 1
	# Help sorting the partitions
	desired_order = ['C', 'pr', 'M', 'E', 'NS1', 'NS2', 'NS3', 'NS4A', 'NS4B', 'NS5']
	ordered_keys = []
	for gene in desired_order:
		for key in dic:
			if f"{gene}.final.fasta" in key:
				ordered_keys.append(key)
				break
			else:
				pass
	# Report ordered partitions
	for key in ordered_keys:
		name = re.compile("([^\/]+)\.final\.fasta").findall(key)[0]
		start = position
		end = dic[key] + start - 1
		position = end + 1
		partitions += "\tcharset {} = {}-{};\n".format(name, start, end)
	partitions += "end;\n"
	handle = open("{}/partitions.nex".format(output), "w")
	handle.write(partitions)
	handle.close()
	return
