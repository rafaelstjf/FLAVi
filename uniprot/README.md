Before executing the FLAVi pipeline, you must prepare the UniProt database like so:

```bash
$ makeblastdb -in uniprot_sprot.pep -parse_seqids -dbtype prot
```
